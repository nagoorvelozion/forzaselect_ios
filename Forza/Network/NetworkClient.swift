//
//  NetworkClient.swift
//  Forza
//
//  Created by Naveen Kumar K N on 31/03/21.

import Foundation
import Alamofire
import CodableAlamofire

class NetworkClient{
    private static func performRequest<T:Decodable>(route:NetworkRouter, decoder: JSONDecoder = JSONDecoder(), completion:@escaping (Result<T>)->Void) -> DataRequest {
        
        return Alamofire.request(route)
            .responseDecodableObject (decoder: decoder){ (response: DataResponse<T>) in
                if let data = response.data, let completeData = String(data: data, encoding: .utf8) {
                    print("\n\n\n Response Received Data: \n \(completeData)\n\n\n")
                }
                print( "\n Request: \(String(describing: response.request))")
                print( "\n Response: \(String(describing: response.response))")
                print( "\n Response: \(String(describing: response.error))")
                print( "\n Response: \(String(describing: response.response?.statusCode))")
                completion(response.result)
            }
    }

    //MARK: - API call for Login
    static func loginAPICall(userName:String,pwd:String,completion:@escaping (Result<LoginModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.login(userName: userName, pwd: pwd), completion: completion)
    }
    //MARK: - API call for Plan Overview List
    static func getOverviewList(completion:@escaping (Result<PlanOverviewModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getOverviewList, completion: completion)
    }
    //MARK: - API call for Notification List
    static func getNotificationList(completion:@escaping (Result<NotificationModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getNotificationList, completion: completion)
    }
    //MARK: - API call for Notification Read or Not
    static func makeNotificationRead(id:String,completion:@escaping (Result<NotificationModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.markNotificationReadOrNot(id: id), completion: completion)
    }
    //MARK: - API call for User profile 
    static func getProfileDetails(completion:@escaping (Result<ProfileModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getUserProfile, completion: completion)
    }
    //MARK: - API call for Carrier List
    static func getCarrierList(completion:@escaping (Result<CarrierListModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getCarrierList, completion: completion)
    }
    //MARK: - API call for Delete Carrier Item
    static func deleteCarrierListItem(id:String,completion:@escaping (Result<CarrierListModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.deleteCarrierItem(id: id), completion: completion)
    }
    //MARK: - API call for Add Carrier Item
    static func addCarrierItem(dataDict:[String:Any],completion:@escaping (Result<CarrierListModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.addCarrier(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Set Carrier Status
    static func setCarrierStatus(dataDict:[String:Any],completion:@escaping (Result<CarrierListModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.setCarrierStatus(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Get Plan Catagories
    static func getPlanCatagories(completion:@escaping (Result<PlanCatagoriesModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getPlanCategory, completion: completion)
    }
    //MARK: - API call for Get Plan Details
    static func getPlanDetails(id:String,completion:@escaping (Result<PlanDetailsModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getPlanDetails(id: id), completion: completion)
    }
    //MARK: - API call for Update Plan Status
    static func updatePlanStatus(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.updatePlanStatus(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Delete Plan
    static func deletePlan(id:String,cat_id:String,completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.deletePlan(id: id, cat_Id: cat_id), completion: completion)
    }
    
    //MARK: - API call for Display Comments
    static func getDisplayComments(planId:String,CarrierId:String,completion:@escaping (Result<CommentModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getDisplayComments(planId: planId, CarrierId: CarrierId), completion: completion)
    }
    //MARK: - API call for Add Comment
    static func addComment(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.addComment(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Assign Carriers
    static func assignCarriers(planId:String,CarrierId:String,completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.assigncarrier(p_id: planId, carrier: CarrierId), completion: completion)
    }
    //MARK: - API call for AddNew Plans
    static func addNewPlans(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.addNewPlan(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Edit profile
    static func editProfile(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.editProfile(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for delete Carrier plan
    static func deleteCarrierPlan(id:String,completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.deleteCarrierPlan(id: id), completion: completion)
    }
    //MARK: - API call for Get Quotattion List
    static func getQuotationList(completion:@escaping (Result<QuotationModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getQuotationsList, completion: completion)
    }
    //MARK: - API call for Get New Quotattion Parameters List
    static func getNewQuoteParameters(id:String,completion:@escaping (Result<NewQuoteModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getNewQuoteParameters(id: id), completion: completion)
    }
    //MARK: - API call for Get Edit Quotattion Parameters List
    static func getEditQuoteParameters(id:String,CategoryId:String,completion:@escaping (Result<EditQuoteModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.getEditQuoteParameters(id: id, category_Id: CategoryId), completion: completion)
    }
    //MARK: - API call for update price
    static func updatePriceQuoteParameters(id:String,value:String,isFromEditName:Bool,completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.updateQuotationPrice(id: id, value: value, isForNameEdit: isFromEditName), completion: completion)
    }
    //MARK: - API call for submit quote
    static func submitQuote(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.submitQuote(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for View Best In Results
    static func viewBestInResults(id:String,completion:@escaping (Result<BestInModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.bestInResult(id: id), completion: completion)
    }
    //MARK: - API call for View Results
    static func viewResults(id:String,completion:@escaping (Result<ViewResultsModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.viewResult(id: id), completion: completion)
    }
    //MARK: - API call for Create New Plans
    static func createNewPlans(dataDict:[String:Any],completion:@escaping (Result<MessageModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.createNewPlans(dataDict: dataDict), completion: completion)
    }
    //MARK: - API call for Display Help List
    static func getHelpListList(completion:@escaping (Result<HelpModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.displyHelpList, completion: completion)
    }
    //MARK: - API call for Submit Help List
    static func submitHelpReasonList(reason:String,completion:@escaping (Result<HelpModel>)->Void) {
        let _ = performRequest(route: NetworkRouter.submitHelp(reason: reason), completion: completion)
    }
}
