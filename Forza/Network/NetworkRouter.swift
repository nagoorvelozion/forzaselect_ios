//
//  NetworkRouter.swift
//  Forza
//
//  Created by Naveen Kumar K N on 31/03/21.

import Foundation
import Alamofire
public typealias CustomHeaders = [String: String]
enum NetworkRouter : URLRequestConvertible {
    
    case login(userName:String,pwd:String)
    case getOverviewList
    case getNotificationList
    case markNotificationReadOrNot(id:String)
    case getUserProfile
    case getCarrierList
    case deleteCarrierItem(id:String)
    case addCarrier(dataDict:[String:Any])
    case setCarrierStatus(dataDict:[String:Any])
    case getPlanCategory
    case getPlanDetails(id:String)
    case updatePlanStatus(dataDict:[String:Any])
    case deletePlan(id:String,cat_Id:String)
    case getDisplayComments(planId:String,CarrierId:String)
    case addComment(dataDict:[String:Any])
    case assigncarrier(p_id:String,carrier:String)
    case addNewPlan(dataDict:[String:Any])
    case editProfile(dataDict:[String:Any])
    case deleteCarrierPlan(id:String)
    case getQuotationsList
    case getNewQuoteParameters(id:String)
    case getEditQuoteParameters(id:String,category_Id:String)
    case updateQuotationPrice(id:String,value:String,isForNameEdit:Bool)
    case submitQuote(dataDict:[String:Any])
    case viewResult(id:String)
    case bestInResult(id:String)
    case createNewPlans(dataDict:[String:Any])
    case displyHelpList
    case submitHelp(reason:String)
    // MARK: - HTTPUrlPath
    private var path : String{
        switch self {
        case .login(let uname,let pwd):
            return "?task=login&email=\(uname)&password=\(pwd)"
        case .getOverviewList:
            return "?task=planoverview"
        case .getNotificationList:
            return "?task=dispnotification"
        case .markNotificationReadOrNot(let id):
            return "?task=mkreadnot&notf_id=\(id)"
        case .getUserProfile:
            return "?task=myprofile"
        case .getCarrierList:
            return "?task=carrierlist"
        case .deleteCarrierItem(let id):
            return "?task=deletecarrrier&p_id=\(id)"
        case .addCarrier:
            return "?task=addcarrier"
        case .setCarrierStatus:
            return "?task=carrierresetstatus"
        case .getPlanCategory:
            return "?task=planscategory"
        case .getPlanDetails(let id):
            return "?task=prtcplanscategory&plan_id=\(id)"
        case .updatePlanStatus:
            return "?task=updtprtcplnscatgry"
        case .deletePlan(let id,let cat_Id):
            return "?task=delcatgprtcplns&p_id=\(id)&cat_id=\(cat_Id)"
        case .getDisplayComments( let planId,let carrierID):
            return "?task=displaycomment&plan_id=\(planId)&career_id=\(carrierID)"
        case .addComment:
            return "?task=addcomment"
        case .assigncarrier(let planId, let CarrierId):
            return "?task=assigncarrier&p_id=\(planId)&carrier=\(CarrierId)"
        case .addNewPlan:
            return "?task=addcatprtcplns"
        case .editProfile:
            return "?task=editprofile"
        case .deleteCarrierPlan(let id):
            return "?task=delplan&p_id=\(id)"
        case .getQuotationsList:
            return "?task=carrierplanoverview"
        case .getNewQuoteParameters(let id):
            return "?task=dsplycarrerquotelst&p_id=\(id)"
        case .getEditQuoteParameters(let id,let category_Id):
            return "?task=edit_quote&product_id=\(id)&category_id=\(category_Id)"
        case .updateQuotationPrice(let id, let value,let isFromNameEdit):
            let urlString : String = value.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? ""
            if isFromNameEdit{
                return "?task=carierprtl_edt_name&product_id=\(id)&value=\(urlString)"
            }
            return "?task=carierprtl_edt_price&cost_id=\(id)&value=\(urlString)"
        case .submitQuote:
            return "?task=submit_quote"
        case .viewResult(let id):
            return "?task=viewresult&type=1&cat_id=\(id)"
        case .bestInResult(let id):
            return "?task=viewresult&type=2&cat_id=\(id)"
        case .createNewPlans:
            return "?task=addplans"
        case .displyHelpList:
            return "?task=displayhelplist"
        case .submitHelp(let reason):
            return "?task=addhelp&information=\(reason)"
        }
    }
    // MARK: - HTTPMethod
    private var method : HTTPMethod{
        switch self {
        case .login,.getOverviewList,.getNotificationList,.markNotificationReadOrNot,.getUserProfile,.getCarrierList,.deleteCarrierItem,.addCarrier,.setCarrierStatus,.getPlanCategory,.getPlanDetails,.updatePlanStatus,.deletePlan,.getDisplayComments,.addComment,.assigncarrier,.addNewPlan,.editProfile,.deleteCarrierPlan,.getQuotationsList,.getNewQuoteParameters,.getEditQuoteParameters,.updateQuotationPrice,.submitQuote,.viewResult,.bestInResult,.createNewPlans,.displyHelpList,.submitHelp:
            return .post
        }
    }
    
    // MARK: - Parameters
    private var parameters:Parameters?{
        switch self {
        case .login,.getOverviewList,.getNotificationList,.markNotificationReadOrNot,.getUserProfile,.getCarrierList,.deleteCarrierItem,.getPlanCategory,.getPlanDetails,.deletePlan,.getDisplayComments,.assigncarrier,.deleteCarrierPlan,.getQuotationsList,.getNewQuoteParameters,.getEditQuoteParameters,.updateQuotationPrice,.viewResult,.bestInResult,.displyHelpList,.submitHelp:
            return nil
        case .addCarrier(let dataDict),.setCarrierStatus(let dataDict),.updatePlanStatus(let dataDict),.addComment(let dataDict),.addNewPlan(let dataDict),.editProfile(let dataDict),.submitQuote(let dataDict),.createNewPlans(let dataDict):
            return dataDict
        }
    }
    
    //MARK:- Custom Headers
    private var customHeaders : CustomHeaders? {
        switch self {
        case .login,.getOverviewList,.getNotificationList,.markNotificationReadOrNot,.getUserProfile,.getCarrierList,.deleteCarrierItem,.addCarrier,.setCarrierStatus,.getPlanCategory,.getPlanDetails,.updatePlanStatus,.deletePlan,.getDisplayComments,.addComment,.assigncarrier,.addNewPlan,.deleteCarrierPlan,.editProfile,.getQuotationsList,.getNewQuoteParameters,.getEditQuoteParameters,.updateQuotationPrice,.submitQuote,.viewResult,.bestInResult,.createNewPlans,.displyHelpList,.submitHelp:
            return nil
        }
    }
    
    //MARK:- HTTPRequest
    func asURLRequest() throws -> URLRequest {
        
        //Creating Request URL
        let pathMut = K.ServerURL.serverbaseURL + path
        let url = try pathMut.asURL()
        var urlRequest =  URLRequest(url: url)
        print("Final UrlRequest :: \(urlRequest)")
        
        // HTTP Method
        urlRequest.httpMethod = method.rawValue
        
        // Basic Oauth
        let loginString = String(format: "%@:%@", UserDefaultsManager.userId, UserDefaultsManager.pwd)
        let loginData = loginString.data(using: String.Encoding.utf8)!
        let base64LoginString = loginData.base64EncodedString()
        
        // Common Headers
        if !path.contains("task=login"){
            urlRequest.setValue("Basic \(base64LoginString)", forHTTPHeaderField: "Authorization")
        }
        urlRequest.setValue("application/x-www-form-urlencoded", forHTTPHeaderField:"Content-Type");

        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField:HTTPHeaderField.acceptType.rawValue)
        urlRequest.setValue(ContentType.json.rawValue, forHTTPHeaderField:HTTPHeaderField.contentType.rawValue)
        
        //Custom Headers
        if let heads = customHeaders {
            for (field, value) in heads {
                urlRequest.setValue(value, forHTTPHeaderField: field)
            }
        }
        
        // Parameters
        if let parameters = parameters {
            //  do {
            let cookieHeader = parameters.map { ($0.0) + "=" + ($0.1 as! String) }.joined(separator: "&")
            urlRequest.httpBody = cookieHeader.data(using: .utf8)
            //urlRequest.httpBody = getFormDataPostString(params: parameters).data(using: .utf8)!
            
            // urlRequest.httpBody = try JSONSerialization.data(withJSONObject: parameters, options: [])
            //            } catch {
            //                throw AFError.parameterEncodingFailed(reason: .jsonEncodingFailed(error: error))
            //            }
        }
        return urlRequest
    }
    
    func getFormDataPostString(params:[String:Any]) -> String{
        var components = URLComponents()
        components.queryItems = params.keys.compactMap{
            URLQueryItem(name: $0, value: params[$0] as? String)
        }
        // let value =
        return (components.query ?? "")
    }
}
