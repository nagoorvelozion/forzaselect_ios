//
//  AppDelegate.swift
//  Forza
//
//  Created by Naveen Kumar K N on 31/03/21.
//

import UIKit
import IQKeyboardManagerSwift
@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.enableAutoToolbar = false
        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = UIColor.white
        navigationBarAppearace.barTintColor = UIColor(named:"PrimaryColor")
        navigationBarAppearace.isTranslucent = false
        navigationBarAppearace.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
        return true
    }
    
    // MARK: UISceneSession Lifecycle
    
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }
    
    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        
    }
    
    
}

extension AppDelegate{
    func setRootViewController(){
        if UserDefaultsManager.isUserLoggedIn == true{
            if UserDefaultsManager.isUserLoggedAsCarrier == true{
                let storyBoard = UIStoryboard(name: "Carrier", bundle: nil)
                let tabBarController: CarrierTabBarViewController = storyBoard.instantiateViewController(withIdentifier: "CarrierTabBarViewController") as! CarrierTabBarViewController
                APP_DELEGATE.window!.rootViewController = tabBarController
                APP_DELEGATE.window!.makeKeyAndVisible()
                return
            }
            let storyBoard = UIStoryboard(name: "Admin", bundle: nil)
            let tabBarController: DashboardTabBarViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardTabBarViewController") as! DashboardTabBarViewController
            APP_DELEGATE.window!.rootViewController = tabBarController
            APP_DELEGATE.window!.makeKeyAndVisible()
        }
    }
}
