//
//  ViewResultsTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class ViewResultsTableViewCell: UITableViewCell {
    @IBOutlet weak var subtitleLbl: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var titleLbl: UILabel!
    var dataArr = [ViewResultsPriceModel](){
        didSet{
            self.collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib1 = UINib(nibName: "ViewResultCollectionViewCell", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "ViewResultCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
extension ViewResultsTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: ViewResultCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewResultCollectionViewCell", for: indexPath) as! ViewResultCollectionViewCell
        cell.valueLbl.text = dataArr[indexPath.row].value ?? ""
        if dataArr[indexPath.row].isbest == 1{
            cell.valueLbl.backgroundColor = #colorLiteral(red: 0, green: 0.6729254723, blue: 0.4588291049, alpha: 1)
        }else{
            cell.valueLbl.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width  / CGFloat(dataArr.count)
        return CGSize(width: width, height: 50.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  0.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
