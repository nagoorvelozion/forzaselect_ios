//
//  NewQuoteDropDownTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 20/04/21.
//

import UIKit

class NewQuoteDropDownTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var ValueLbl: UILabel!
    
    @IBOutlet weak var dropdownBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
