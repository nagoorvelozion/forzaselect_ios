//
//  AddPlanHeaderTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class AddPlanHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var minusBtn: UIButton!
    @IBOutlet weak var plusBtn: UIButton!
    @IBOutlet weak var dropDownBtn: UIButton!
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var textLbl: UILabel!
    var textFieldValue : ((String,Int)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTF.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}

extension AddPlanHeaderTableViewCell : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        self.textFieldValue?(textField.text!, textField.tag)
        return true
    }
}
