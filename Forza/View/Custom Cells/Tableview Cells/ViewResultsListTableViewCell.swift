//
//  ViewResultsListTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class ViewResultsListTableViewCell: UITableViewCell {

    @IBOutlet weak var collectionView: UICollectionView!
    var dataArr = [ViewResultsCarrierModel](){
        didSet{
            self.collectionView.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib1 = UINib(nibName: "ViewResultsMainCollectionViewCell", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "ViewResultsMainCollectionViewCell")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}

extension ViewResultsListTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: ViewResultsMainCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ViewResultsMainCollectionViewCell", for: indexPath) as! ViewResultsMainCollectionViewCell
        var imageString = dataArr[indexPath.row].profile_img ?? ""
        if imageString.contains(" "){
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        }
        cell.imageVW.sd_setImage(with:URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        cell.nameLbl.text = dataArr[indexPath.row].name ?? ""
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.collectionView.frame.width  / CGFloat(dataArr.count)
        return CGSize(width: width, height: 170.0)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  0.0
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0.0
    }
}
