//
//  ProfileImageTableViewCell.swift
//  PadhYatra
//
//  Created by Naveen Kumar K N on 29/01/21.
//

import UIKit

class ProfileImageTableViewCell: UITableViewCell {
    @IBOutlet weak var editBtn: UIButton!
    
    @IBOutlet weak var profileImg: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        self.profileImg.layer.borderWidth = 1.0
        self.profileImg.layer.masksToBounds = false
        self.profileImg.layer.borderColor = #colorLiteral(red: 0.921431005, green: 0.9214526415, blue: 0.9214410186, alpha: 1)
        self.profileImg.layer.cornerRadius = self.profileImg.frame.size.width/2
        self.profileImg.clipsToBounds = true
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
