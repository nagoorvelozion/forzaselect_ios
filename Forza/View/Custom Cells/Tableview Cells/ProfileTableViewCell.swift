//
//  ProfileTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class ProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var imageVw: UIImageView!
    @IBOutlet weak var contentLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
