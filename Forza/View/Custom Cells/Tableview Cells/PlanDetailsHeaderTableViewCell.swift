//
//  PlanDetailsHeaderTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class PlanDetailsHeaderTableViewCell: UITableViewCell {

    @IBOutlet weak var updateBtnHtConst: NSLayoutConstraint!
    @IBOutlet weak var textLbl: UILabel!
    @IBOutlet weak var dropdownBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    var textfieldResign : ((String)->())?
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTF.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PlanDetailsHeaderTableViewCell:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        self.updateBtnHtConst.constant = 80.0
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.textfieldResign?(textField.text!)
        textField.resignFirstResponder()
        return true
    }
}
