//
//  QuotationProductTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import UIKit

class QuotationProductTableViewCell: UITableViewCell {

    @IBOutlet weak var editQuoteBtn: UIButton!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var statusLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
