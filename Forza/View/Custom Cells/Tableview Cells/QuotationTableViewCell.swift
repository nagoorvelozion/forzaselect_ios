//
//  QuotationTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import UIKit

class QuotationTableViewCell: UITableViewCell {
    
    @IBOutlet weak var tableViewHtConstratint: NSLayoutConstraint! // 0
    @IBOutlet weak var tableVW: UITableView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var commentBtn: SSBadgeButton!
    @IBOutlet weak var newQuoteBtn: UIButton!
    @IBOutlet weak var titleVW: UILabel!
    var status = ""
    var editClicked : ((String,String)->())?
    var quotationArr = [QuotationProductModel](){
        didSet{
            self.tableViewHtConstratint.constant = (CGFloat(self.quotationArr.count) * 40.0)
            self.tableVW.reloadData()
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        commentBtn.badgeEdgeInsets = UIEdgeInsets(top: 15, left: 0, bottom: 0, right: 10)
        commentBtn.badgeBackgroundColor = UIColor(named: "PrimaryColor")!
        tableVW.register(UINib(nibName: "QuotationProductTableViewCell",bundle: nil), forCellReuseIdentifier: "QuotationProductTableViewCell")
        tableVW.delegate = self
        tableVW.dataSource = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    @objc func editBtnClicked(sender:UIButton){
        self.editClicked?(self.quotationArr[sender.tag].product_id ?? "",self.quotationArr[sender.tag].product_name ?? "")
    }
}

extension QuotationTableViewCell:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.quotationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuotationProductTableViewCell", for: indexPath) as! QuotationProductTableViewCell
        let item = self.quotationArr[indexPath.row]
        cell.statusLbl.text = "  " + self.status + "  "
        cell.titleLbl.text = "  " + (item.product_name?.uppercased() ?? "") + "  "
        cell.editQuoteBtn.addTarget(self, action: #selector(editBtnClicked), for: .touchUpInside)
        cell.editQuoteBtn.tag = indexPath.row
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 40.0
    }
}
