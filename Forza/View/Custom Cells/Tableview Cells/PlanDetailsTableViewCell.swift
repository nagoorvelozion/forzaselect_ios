//
//  PlanDetailsTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class PlanDetailsTableViewCell: UITableViewCell {

    @IBOutlet weak var updateBtnWidthConst: NSLayoutConstraint! // 80.0
    @IBOutlet weak var categoryValueLbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var categoryBtn: UIButton!
    @IBOutlet weak var lowHighBtn: UIButton!
    @IBOutlet weak var nameTF: UITextField!
   
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTF.delegate = self
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension PlanDetailsTableViewCell:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
    }
}
