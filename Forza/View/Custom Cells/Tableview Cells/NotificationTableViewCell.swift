//
//  NotificationTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class NotificationTableViewCell: UITableViewCell {
    @IBOutlet weak var statusImageVw: UIImageView!
    @IBOutlet weak var contentLbl: UILabel!
    
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var imageVw: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageVw.layer.cornerRadius = imageVw.frame.height/2
        imageVw.layer.borderWidth = 1.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
