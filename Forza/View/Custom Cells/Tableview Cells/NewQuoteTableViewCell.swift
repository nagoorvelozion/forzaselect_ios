//
//  NewQuoteTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import UIKit

class NewQuoteTableViewCell: UITableViewCell {
    let numberToolbar: UIToolbar = UIToolbar()
    @IBOutlet weak var valueTF: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
          numberToolbar.items=[
            UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(CancelClicked)),
            UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.bordered, target: self, action: #selector(doneClicked))
          ]

          numberToolbar.sizeToFit()

        valueTF.inputAccessoryView = numberToolbar    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   @objc func doneClicked () {
        valueTF.resignFirstResponder()
    }

   @objc func CancelClicked () {
        valueTF.resignFirstResponder()
    }
}
