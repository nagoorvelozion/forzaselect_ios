//
//  EditWuoteDropDownTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 20/04/21.
//

import UIKit

class EditWuoteDropDownTableViewCell: UITableViewCell {
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    @IBOutlet weak var dropdownBtn: UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
