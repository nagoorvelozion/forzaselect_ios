//
//  AssignCarrierTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class AssignCarrierTableViewCell: UITableViewCell {

    @IBOutlet weak var selectionImage: UIImageView!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageVW: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageVW.layer.cornerRadius = imageVW.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
