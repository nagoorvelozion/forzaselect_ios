//
//  CommentTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var commentLbl: UILabel!
    @IBOutlet weak var nameLbel: UILabel!
    @IBOutlet weak var profileImgVW: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        profileImgVW.layer.cornerRadius = profileImgVW.frame.height/2
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
