//
//  HelpTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit
import IQKeyboardManagerSwift

class HelpTableViewCell: UITableViewCell {

    @IBOutlet weak var submitBtn: UIButton!
    @IBOutlet weak var reasonTW: IQTextView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
