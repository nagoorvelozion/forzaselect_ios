//
//  OverviewTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import UIKit
import SDWebImage
class OverviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var cLblHtConst: NSLayoutConstraint!
    @IBOutlet weak var collectionVWHtConst: NSLayoutConstraint!
    @IBOutlet weak var viewResultBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var statusBtn: UIButton!
    @IBOutlet weak var viewBestInClassBtn: UIButton!
    @IBOutlet weak var assignBtn: UIButton!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var dateLbl: UILabel!
    @IBOutlet weak var titleLbl: UILabel!
    var commentBtnClicked : ((PlanOverviewProfileDetails)->())?
    var dataArr = [PlanOverviewProfileDetails](){
        didSet{
            self.collectionView.delegate = self
            self.collectionView.dataSource = self
            self.collectionView.reloadData()
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let nib1 = UINib(nibName: "OverviewCollectionViewCell", bundle: nil)
        collectionView?.register(nib1, forCellWithReuseIdentifier: "OverviewCollectionViewCell")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
        let size = CGSize(width: 200, height: 1000) // temporary size
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        return NSString(string: text).boundingRect(with: size,
                                                   options: options,
                                                   attributes: [NSAttributedString.Key.font: font],
                                                   context: nil)
    }
    @objc func commentBtnAction(sender:UIButton){
        self.commentBtnClicked?(dataArr[sender.tag])
    }
 
}

extension OverviewTableViewCell:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell: OverviewCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "OverviewCollectionViewCell", for: indexPath) as! OverviewCollectionViewCell
        cell.nameLbl.text = dataArr[indexPath.row].name ?? ""
        var imageString = dataArr[indexPath.row].profile_img ?? ""
        if imageString.contains(" "){
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        }
        cell.imageVW.sd_setImage(with:URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        cell.commentBtn.badge = dataArr[indexPath.row].count ?? ""
        cell.commentBtn.addTarget(self, action: #selector(commentBtnAction), for: .touchUpInside)
        cell.commentBtn.tag = indexPath.row
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = self.estimatedFrame(text: dataArr[indexPath.row].name ?? "", font: UIFont.systemFont(ofSize: 15)).width + 70
        return CGSize(width: width, height: 40.0)
    }
    
//    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
//        return UIEdgeInsets(top: 10, left: 0, bottom: 10, right: 0)
//    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  10.0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10.0
    }
}
