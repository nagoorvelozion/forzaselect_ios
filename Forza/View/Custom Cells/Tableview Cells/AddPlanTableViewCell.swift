//
//  AddPlanTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class AddPlanTableViewCell: UITableViewCell {
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var categoryDropDownBtn: UIButton!
    @IBOutlet weak var categoryLbl: UILabel!
    @IBOutlet weak var statusDropDownBtn: UIButton!
    @IBOutlet weak var statusLbl: UILabel!
   
    @IBOutlet weak var minusBtn: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        titleTF.delegate = self
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
extension AddPlanTableViewCell:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
}
