//
//  CarrierListingTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class CarrierListingTableViewCell: UITableViewCell {

    @IBOutlet weak var enableLbl: UILabel!
    @IBOutlet weak var enableBtn: UIButton!
    @IBOutlet weak var deleteBtn: UIButton!
    @IBOutlet weak var passwordLbl: UILabel!
    @IBOutlet weak var emailLbl: UILabel!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var imageVW: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageVW.layer.cornerRadius = imageVW.frame.height/2

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
