//
//  EditQuoteTableViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 11/04/21.
//

import UIKit

class EditQuoteTableViewCell: UITableViewCell {
    @IBOutlet weak var valueTF: UITextField!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var saveBtn: UIButton!
    let numberToolbar: UIToolbar = UIToolbar()

    override func awakeFromNib() {
        super.awakeFromNib()
        numberToolbar.items=[
            UIBarButtonItem(title: "Cancel", style: UIBarButtonItem.Style.plain, target: self, action: #selector(CancelClicked)),
          UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: self, action: nil),
            UIBarButtonItem(title: "Done", style: UIBarButtonItem.Style.plain, target: self, action: #selector(doneClicked))
        ]
        numberToolbar.sizeToFit()

      valueTF.inputAccessoryView = numberToolbar
    }
    @objc func doneClicked () {
         valueTF.resignFirstResponder()
     }

    @objc func CancelClicked () {
         valueTF.resignFirstResponder()
     }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
