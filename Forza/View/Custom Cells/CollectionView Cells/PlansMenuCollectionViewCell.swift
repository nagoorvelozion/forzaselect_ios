//
//  PlansMenuCollectionViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class PlansMenuCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var highlightedView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    override var isHighlighted: Bool {
        didSet {
        }
    }
    
    override var isSelected: Bool {
        didSet {
 
        }
    }
}
