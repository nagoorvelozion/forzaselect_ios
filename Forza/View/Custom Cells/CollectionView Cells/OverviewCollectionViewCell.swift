//
//  OverviewCollectionViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class OverviewCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageVW: UIImageView!
    @IBOutlet weak var commentBtn: SSBadgeButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageVW.layer.cornerRadius = imageVW.frame.height/2
        imageVW.layer.borderWidth = 1.0
        commentBtn.badgeEdgeInsets = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 10)
        commentBtn.badgeBackgroundColor = UIColor(named: "PrimaryColor")!
       // commentBtn.badgeBackgroundColor = .clear
       // commentBtn.badgeTextColor = UIColor(named: "PrimaryColor")!
       // commentBtn.badgeFont = UIFont.systemFont(ofSize: 14, weight: .black)
    }
}
