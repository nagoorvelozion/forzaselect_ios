//
//  ViewResultsMainCollectionViewCell.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class ViewResultsMainCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var imageVW: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        imageVW.layer.cornerRadius = imageVW.frame.height/2
    }
}
