//
//  QuotationsViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import UIKit

class QuotationsViewController: BaseViewController {
    var viewModel = QuotationsViewModel()
    @IBOutlet weak var tableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "QuotationTableViewCell",bundle: nil), forCellReuseIdentifier: "QuotationTableViewCell")
        
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
    }
    func bindViewModel(){
        viewModel.getNotificationList()
        viewModel.getQuotationsList()
        viewModel.success = { message, state in
            if state{
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
        viewModel.notificationSuccess = { count in
            UserDefaultsManager.notificationCount = count
            self.customNavigationType = .navWithBellandLogout
        }
    }
    override func logoutSelected(sender: UIButton) {
        UserDefaultsManager.isUserLoggedIn = false
        let story = UIStoryboard(name: "Main", bundle:nil)
        let navigationController:UINavigationController = story.instantiateInitialViewController() as! UINavigationController
        let statScreenVC = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
        navigationController.viewControllers = [statScreenVC]
        APP_DELEGATE.window?.rootViewController = navigationController
        APP_DELEGATE.window?.makeKeyAndVisible()
    }
    @objc func commentBtnClicked(sender:UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
        let item = viewModel.quotationArr[sender.tag]
        vc.carrierId = item.carrier ?? ""
        vc.planId = item.user_id ?? ""
        vc.name = item.name?.capitalized ?? ""
        vc.onDismiss = {
            self.viewModel.getQuotationsList()
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    @objc func newQuotaBtnClicked(sender:UIButton){
    let storyboard: UIStoryboard = UIStoryboard(name: "Carrier", bundle: nil)
    let vc = storyboard.instantiateViewController(withIdentifier: "NewQuoteViewController") as! NewQuoteViewController
        vc.id = viewModel.quotationArr[sender.tag].id ?? ""
        vc.onDismiss = { message in
            self.viewModel.getQuotationsList()
            Utility.showAlertWith(message: message, title: "", forController: self)
        }
    vc.modalPresentationStyle = .overCurrentContext
    vc.modalTransitionStyle = .crossDissolve
    vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
    self.tabBarController?.present(vc, animated: true, completion: nil)
    }
}

extension QuotationsViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.quotationArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "QuotationTableViewCell", for: indexPath) as! QuotationTableViewCell
        let item = viewModel.quotationArr[indexPath.row]
        cell.titleVW.text = item.name ?? ""
        cell.dateLbl.text = item.date ?? ""
        cell.commentBtn.badge = item.count ?? "0"
        cell.status = item.status ?? ""
        cell.quotationArr = item.products ?? []
        cell.commentBtn.addTarget(self, action: #selector(commentBtnClicked), for: .touchUpInside)
        cell.commentBtn.tag = indexPath.row
        cell.newQuoteBtn.addTarget(self, action: #selector(newQuotaBtnClicked), for: .touchUpInside)
        cell.newQuoteBtn.tag = indexPath.row
        cell.editClicked = { productId,productName in
            
            let storyboard: UIStoryboard = UIStoryboard(name: "Carrier", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "EditQuoteViewController") as! EditQuoteViewController
            vc.id = productId
            vc.categoryId = item.category_id ?? ""
            vc.titleText = productName
            vc.onDismiss = {
                self.viewModel.getNotificationList()
                self.viewModel.getQuotationsList()
            }
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.tabBarController?.present(vc, animated: true, completion: nil)
        }
        return cell
    }
}
