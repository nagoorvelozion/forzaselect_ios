//
//  EditQuoteViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 11/04/21.
//

import UIKit
import DropDown
class EditQuoteViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var titleTF: UITextField!
    var viewModel = EditQuoteViewModel()
    let dropDown = DropDown()
    var id = ""
    var categoryId = ""
    var titleText = ""
    var onDismiss : (()->())?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "EditQuoteTableViewCell",bundle: nil), forCellReuseIdentifier: "EditQuoteTableViewCell")
        tableView.register(UINib(nibName: "NewQuoteHeaderTableViewCell",bundle: nil), forCellReuseIdentifier: "NewQuoteHeaderTableViewCell")
        tableView.register(UINib(nibName: "EditWuoteDropDownTableViewCell",bundle: nil), forCellReuseIdentifier: "EditWuoteDropDownTableViewCell")
        self.titleTF.text = "  " + self.titleText
        updateBtn.addTarget(self, action: #selector(updateBtnClicked), for: .touchUpInside)
        bindViewModel()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.dismiss(animated: true, completion: {self.onDismiss?()})
    }
    func bindViewModel(){
        viewModel.getEditQuoteList(id: id, CatId: categoryId)
        viewModel.success = { message, state in
            if state {
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
        viewModel.updateSuccess = { message in
            self.viewModel.getEditQuoteList(id: self.id, CatId: self.categoryId)
            Utility.showAlertWith(message: message, title: "", forController: self)
        }
    }
    @objc func saveBtnClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let item = viewModel.editQuoteArr[indexPath!.section].products?[indexPath!.row]
        self.viewModel.updateProductPrice(id: item?.cost_id ?? "", value: item?.cost_price ?? "", isFromEditName: false)
    }
    @objc func updateBtnClicked(sender:UIButton){
        if titleTF.text == "" || titleTF.text == "  "{
            Utility.showAlertWith(message: "Please enter title", title: "", forController: self)
            return
        }
        self.viewModel.updateProductPrice(id: id, value: titleTF.text!, isFromEditName: true)
    }
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    @objc func dropDownSelected(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Yes","No"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.editQuoteArr[indexPath!.section].products?[indexPath!.row].cost_price = item
            self.dropDown.hide()
            self.tableView.reloadData()
        }
        self.dropDown.show()
    }
}

extension EditQuoteViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.editQuoteArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.editQuoteArr[section].products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditQuoteTableViewCell", for: indexPath) as! EditQuoteTableViewCell
        let item = viewModel.editQuoteArr[indexPath.section].products?[indexPath.row]
        cell.titleLbl.text = item?.name ?? ""
        cell.valueTF.text = item?.cost_price ?? ""
        switch item?.cat_status {
        case "0":
            cell.valueTF.placeholder = "Price"
        case "1":
            cell.valueTF.placeholder = "Age range(12-34)"
        case "3":
            cell.valueTF.placeholder = "Day range(12-34)"
        case "2":
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "EditWuoteDropDownTableViewCell", for: indexPath) as! EditWuoteDropDownTableViewCell
            cell1.titleLbl.text = item?.name ?? ""
            cell1.valueLbl.text = item?.cost_price ?? ""
            item?.cost_price == nil ? (cell1.valueLbl.textColor = .systemGray2) : (cell1.valueLbl.textColor = .label)
            cell1.dropdownBtn.addTarget(self, action: #selector(dropDownSelected), for: .touchUpInside)
            cell1.saveBtn.addTarget(self, action: #selector(saveBtnClicked), for: .touchUpInside)
            return cell1
        default:
            cell.valueTF.placeholder = "Month range(1-12)"
        }
        cell.saveBtn.addTarget(self, action: #selector(saveBtnClicked), for: .touchUpInside)
        cell.valueTF.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "NewQuoteHeaderTableViewCell") as! NewQuoteHeaderTableViewCell
        headerCell.titleLbl.text = viewModel.editQuoteArr[section].name ?? ""
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}

extension EditQuoteViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        viewModel.editQuoteArr[indexPath!.section].products?[indexPath!.row].cost_price = textField.text!
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        viewModel.editQuoteArr[indexPath!.section].products?[indexPath!.row].cost_price = textField.text!
        textField.resignFirstResponder()
    }
}
