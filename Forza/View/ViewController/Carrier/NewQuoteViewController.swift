//
//  NewQuoteViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import UIKit
import DropDown
class NewQuoteViewController: UIViewController {
    
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let dropDown = DropDown()
    var viewModel = QuotationsViewModel()
    var id = ""
    var onDismiss : ((String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "NewQuoteTableViewCell",bundle: nil), forCellReuseIdentifier: "NewQuoteTableViewCell")
        tableView.register(UINib(nibName: "NewQuoteHeaderTableViewCell",bundle: nil), forCellReuseIdentifier: "NewQuoteHeaderTableViewCell")
        tableView.register(UINib(nibName: "NewQuoteDropDownTableViewCell",bundle: nil), forCellReuseIdentifier: "NewQuoteDropDownTableViewCell")
        self.titleTF.delegate = self
        bindViewModel()
    }
    func bindViewModel(){
        viewModel.getNewQuotesParameters(id: id)
        viewModel.success = { message , state in
            if state{
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
        viewModel.submitQuoteSuccess = { message , state in
            if state{
                self.dismiss(animated: true, completion: {self.onDismiss?(message)})
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
                ProgressView.stopLoader()
            }
        }
    }
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.dismiss(animated: true, completion: nil)
    }
    @objc func dropDownSelected(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Yes","No"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.viewModel.newQuoteArr[indexPath!.section].products?[indexPath!.row].value = item
            self.dropDown.hide()
            self.tableView.reloadData()
        }
        self.dropDown.show()

    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if titleTF.text == ""{
            Utility.showAlertWith(message: "Please enter title", title: "", forController: self)
            return
        }
        var obj = NewQuoteSubmitModel()
        obj.data = [NewQuoteProductSubmitModel]()
        var value = NewQuoteProductSubmitModel()
        for item in self.viewModel.newQuoteArr{
            for data in item.products ?? []{
                if data.value == nil || data.value == ""{
                    Utility.showAlertWith(message: "\(data.name ?? "") is empty", title: "", forController: self)
                    return
                }else{
                    value.name = data.name ?? ""
                    value.price = data.value ?? ""
                    value.products_id = data.id ?? ""
                    obj.data?.append(value)
                }
            }
        }
        
        
        do{
            let jsonData = try obj.jsonData()
            guard let jsonString = String(data: jsonData, encoding: .utf8) else {
                return
            }
            print("================================\n\(jsonString)\n===========================================")
            let dataDict : [String:Any] = ["data" : jsonString,"product_name" : self.titleTF.text!,"cat_id" : self.id]
            viewModel.submitNewQuotesParameters(dataDict: dataDict)
        }catch(let err){
            Utility.showAlertWith(message: err.localizedDescription, title: "", forController: self)
        }

    }
    
}

extension NewQuoteViewController:UITableViewDataSource,UITableViewDelegate{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.newQuoteArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.newQuoteArr[section].products?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NewQuoteTableViewCell", for: indexPath) as! NewQuoteTableViewCell
        let item = viewModel.newQuoteArr[indexPath.section].products?[indexPath.row]
        cell.titleLbl.text = item?.name ?? ""
        cell.valueTF.text = item?.value ?? ""
        switch item?.cat_status {
        case "0":
            cell.valueTF.placeholder = "Price"
        case "1":
            cell.valueTF.placeholder = "Age range(12-34)"
        case "3":
            cell.valueTF.placeholder = "Day range(12-34)"
        case "2":
            let cell1 = tableView.dequeueReusableCell(withIdentifier: "NewQuoteDropDownTableViewCell", for: indexPath) as! NewQuoteDropDownTableViewCell
            cell1.titleLbl.text = item?.name ?? ""
            cell1.ValueLbl.text = item?.value ?? "Yes/No"
            item?.value == nil ? (cell1.ValueLbl.textColor = .systemGray2) : (cell1.ValueLbl.textColor = .label)
            cell1.dropdownBtn.addTarget(self, action: #selector(dropDownSelected), for: .touchUpInside)
            return cell1
        default:
            cell.valueTF.placeholder = "Month range(1-12)"
        }
        cell.valueTF.delegate = self
        return cell
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "NewQuoteHeaderTableViewCell") as! NewQuoteHeaderTableViewCell
        headerCell.titleLbl.text = viewModel.newQuoteArr[section].name ?? ""
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
}

extension NewQuoteViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField != self.titleTF{
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        self.viewModel.newQuoteArr[indexPath!.section].products?[indexPath!.row].value = textField.text!
            self.tableView.reloadData()
        }
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        if textField != self.titleTF{
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        self.viewModel.newQuoteArr[indexPath!.section].products?[indexPath!.row].value = textField.text!
            self.tableView.reloadData()
        }
        textField.resignFirstResponder()
    }
}
