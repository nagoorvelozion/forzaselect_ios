//
//  LoginViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import UIKit

class LoginViewController: BaseViewController {
    var viewModel = LoginViewModel()
    @IBOutlet weak var pwdTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationType = .navPlain
        // setNavBarTransparent()
        emailTF.layer.borderWidth = 1.0
        emailTF.layer.cornerRadius = 10.0
        emailTF.layer.borderColor = UIColor.white.cgColor
        pwdTF.layer.borderWidth = 1.0
        pwdTF.layer.cornerRadius = 10.0
        pwdTF.layer.borderColor = UIColor.white.cgColor
        self.navigationController?.isNavigationBarHidden = true
        emailTF.delegate = self
        pwdTF.delegate = self
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        bindViewModel()
        
    }
    func bindViewModel(){
        viewModel.loginSuccess = { state in
            if state{
                UserDefaultsManager.isUserLoggedIn = true
                UserDefaultsManager.isUserLoggedAsCarrier = false
                let storyBoard = UIStoryboard(name: "Admin", bundle: nil)
                let tabBarController: DashboardTabBarViewController = storyBoard.instantiateViewController(withIdentifier: "DashboardTabBarViewController") as! DashboardTabBarViewController
                APP_DELEGATE.window!.rootViewController = tabBarController
                APP_DELEGATE.window!.makeKeyAndVisible()
            }else{
                Utility.showAlertWith(message: "Login Failed", title: "", forController: self)
            }
        }
        viewModel.carrierLoginSuccess = {
            UserDefaultsManager.isUserLoggedIn = true
            UserDefaultsManager.isUserLoggedAsCarrier = true
            let storyBoard = UIStoryboard(name: "Carrier", bundle: nil)
            let tabBarController: CarrierTabBarViewController = storyBoard.instantiateViewController(withIdentifier: "CarrierTabBarViewController") as! CarrierTabBarViewController
            APP_DELEGATE.window!.rootViewController = tabBarController
            APP_DELEGATE.window!.makeKeyAndVisible()
        }
    }
    
    @IBAction func rememberBtnClicked(_ sender: UIButton) {
        if !sender.isSelected{
            let image = UIImage(named: "checkIcon")
            sender.isSelected = true
            sender.setImage(image?.withRenderingMode(.alwaysOriginal).withTintColor(UIColor(named: "PrimaryColor")!), for: .normal)
            sender.backgroundColor = .white
        }else{
            let image = UIImage(named: "checkbox_unchecked")
            sender.isSelected = false
            sender.setImage(image?.withRenderingMode(.alwaysOriginal).withTintColor(.white), for: .normal)
            sender.backgroundColor = .clear
        }
    }
    
    @IBAction func loginBtnClicked(_ sender: UIButton) {
        if emailTF.text?.count == 0{
            Utility.showAlertWith(message: "Enter Email", title: "", forController: self)
            return
        }
        if pwdTF.text?.count == 0{
            Utility.showAlertWith(message: "Enter Password", title: "", forController: self)
            return
        }
        viewModel.callLoginAPI(userName: emailTF.text!, pwd: pwdTF.text!)
    }
}

extension LoginViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        textField.resignFirstResponder()
    }
}
