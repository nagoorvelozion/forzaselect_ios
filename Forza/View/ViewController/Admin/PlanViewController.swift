//
//  PlanViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit
import DropDown

class PlanViewController: BaseViewController {
    
    @IBOutlet weak var menuCollectionView: UICollectionView!
    @IBOutlet weak var tableView: UITableView!
    var viewModel = PlanCatagoriesViewModel()
    let dropDown = DropDown()
    var indexPath : IndexPath?
    var isUpdateEnabled = false
    var section : Int?
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationType = .navPlain
        setUpCollectionView()
        bindViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getAllPlanCatagories()
    }
    
    var flowLayout: UICollectionViewFlowLayout {
        let flowLayout = menuCollectionView.collectionViewLayout as! UICollectionViewFlowLayout
        flowLayout.sectionInsetReference = .fromSafeArea
        return flowLayout
    }
    
    func setUpCollectionView(){
        menuCollectionView.clipsToBounds = true
        flowLayout.estimatedItemSize = CGSize(width: 1.0, height: 1.0)
        flowLayout.minimumInteritemSpacing = 8
        tableView.register(UINib(nibName: "PlanDetailsHeaderTableViewCell",bundle: nil), forCellReuseIdentifier: "PlanDetailsHeaderTableViewCell")
        tableView.register(UINib(nibName: "PlanDetailsTableViewCell",bundle: nil), forCellReuseIdentifier: "PlanDetailsTableViewCell")
        let nib1 = UINib(nibName:  "PlansMenuCollectionViewCell", bundle: nil)
        menuCollectionView?.register(nib1, forCellWithReuseIdentifier:"PlansMenuCollectionViewCell")
    }
    
    func bindViewModel(){
        viewModel.success = { state in
            self.menuCollectionView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.planDetailSuccess = { state in
            self.indexPath = nil
            self.isUpdateEnabled = false
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.updateSuccess = { message in
            Utility.showAlertWith(message: message, title: "", forController: self)
        }
    }
    @IBAction func addNewPlanBtnClicked(_ sender: Any) {
        var items = [String]()
        for item in viewModel.planDetailsArr{
            items.append(item.name ?? "")
        }
        items.insert("Select sub plan", at: 0)
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddBenefitViewController") as! AddBenefitViewController
        vc.planDetailsArr = viewModel.planDetailsArr
        vc.subPlanArr = items
        vc.id = viewModel.id
        vc.onDismiss = { message in
            self.viewModel.getPlanDetails(id: self.viewModel.id)
            Utility.showAlertWith(message: message, title: "", forController: self)
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func assignCarrierBtnClicked(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AssignViewController") as! AssignViewController
        vc.id = viewModel.id
        //        vc.profile = viewModel.dataArr[sender.tag].profile ?? []
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    @IBAction func createPlanBtnClicked(_ sender: Any) {
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AddPlansViewController") as! AddPlansViewController
        vc.onDismiss = { message in
            Utility.showAlertWith(message: message, title: "", forController: self)
            self.viewModel.getAllPlanCatagories()
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    
}

extension PlanViewController:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.categoryListArr.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PlansMenuCollectionViewCell", for: indexPath) as! PlansMenuCollectionViewCell
        cell.titleLbl.text = viewModel.categoryListArr[indexPath.row].name?.capitalized ?? ""
        if viewModel.categoryListArr[indexPath.row].isSelected == true{
            cell.highlightedView.isHidden = false
            cell.titleLbl.textColor = UIColor(named: "PrimaryColor")
        }else{
            cell.highlightedView.isHidden = true
            cell.titleLbl.textColor = .separator
        }
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        for (index,_) in viewModel.categoryListArr.enumerated(){
            viewModel.categoryListArr[index].isSelected = false
        }
        viewModel.categoryListArr[indexPath.row].isSelected = true
        self.menuCollectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        self.menuCollectionView.reloadData()
        viewModel.getPlanDetails(id: viewModel.categoryListArr[indexPath.row].id ?? "")
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let collectionViewWidth = collectionView.bounds.width //UIScreen.main.bounds.size.width
        let width = ( collectionViewWidth / 1) - self.flowLayout.minimumInteritemSpacing
        return CGSize(width: width, height: menuCollectionView.frame.size.height)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
}

extension PlanViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.planDetailsArr.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.planDetailsArr[section].product?.count ?? 0
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "PlanDetailsHeaderTableViewCell") as! PlanDetailsHeaderTableViewCell
        let item = viewModel.planDetailsArr[section]
        if self.isUpdateEnabled == true {
            if self.section == section{
            headerCell.updateBtnHtConst.constant = 80.0
            }else{
                headerCell.updateBtnHtConst.constant = 0.0
            }
        }else{
            headerCell.updateBtnHtConst.constant = 0.0
        }
        headerCell.nameTF.text = (item.name ?? "")
        if item.cat_status == "0"{
            headerCell.textLbl.text = "Normal Benifits"
        }else{
            headerCell.textLbl.text = "Premiums"
        }
        
        headerCell.deleteBtn.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        headerCell.dropdownBtn.addTarget(self, action: #selector(dropDownBtnClicked), for: .touchUpInside)
       headerCell.updateBtn.addTarget(self, action: #selector(updateBtnHeaderClicked), for: .touchUpInside)
        
        headerCell.deleteBtn.tag = section
        headerCell.dropdownBtn.tag = section
        headerCell.updateBtn.tag = section
        headerCell.textfieldResign = { value in
            self.viewModel.planDetailsArr[section].name = value
            self.isUpdateEnabled = true
            self.section = section
            self.tableView.reloadData()
        }
        return headerCell
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PlanDetailsTableViewCell", for: indexPath) as! PlanDetailsTableViewCell
        let item = viewModel.planDetailsArr[indexPath.section].product?[indexPath.row]
        if self.indexPath == nil{
            cell.updateBtnWidthConst.constant = 0.0
        }else{
            if self.indexPath == indexPath{
                cell.updateBtnWidthConst.constant = 80.0
                cell.nameTF.becomeFirstResponder()
            }else{
                cell.updateBtnWidthConst.constant = 0.0
            }
        }
        cell.categoryBtn.tag = indexPath.section
        cell.lowHighBtn.tag = indexPath.section
        cell.categoryBtn.addTarget(self, action: #selector(categoryBtnClicked), for: .touchUpInside)
        cell.lowHighBtn.addTarget(self, action: #selector(bestInBtnClicked), for: .touchUpInside)
        cell.nameTF.text = (item?.name ?? "")
        
        if item?.status == "0"{
            cell.valueLbl.text = "Low"
        }else{
            cell.valueLbl.text = "High"
        }
        switch item?.cat_status {
        case "0":
            cell.categoryValueLbl.text = "Price"
        case "1":
            cell.categoryValueLbl.text = "Age Range"
        case "2":
            cell.categoryValueLbl.text = "Yes/No"
        case "3":
            cell.categoryValueLbl.text = "Day Range"
        default:
            cell.categoryValueLbl.text = "Month Range"
        }
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        cell.deleteBtn.tag = 101
        cell.nameTF.delegate = self
        cell.updateBtn.addTarget(self, action: #selector(updateBtnClicked), for: .touchUpInside)
           
        return cell
    }
}

extension PlanViewController{
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    
    @objc func deleteBtnClicked(sender:UIButton){
        Utility.showAlertWithCompletion(message: "Are you sure to delete this plan?", title: "", okTitle: "Yes", cancelTitle: "NO", forController: self) { (state) in
            if state{
                let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
                let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
                if sender.tag == 101{
                let value = self.viewModel.planDetailsArr[indexPath!.section].product?[indexPath!.row]
                    self.viewModel.deletePlan(id: value?.id ?? "", cat_id: value?.category_id ?? "")
                }else{
                    let item = self.viewModel.planDetailsArr[sender.tag]
                    self.viewModel.deletePlan(id: item.id ?? "", cat_id: item.category_id ?? "")
                }
            }
        }
 
    }
    @objc func updateBtnHeaderClicked(sender:UIButton){
        let value = viewModel.planDetailsArr[sender.tag]
        let dataDict : [String:Any] = ["row": "name" ,"p_id": value.id ?? "","value" : value.name ?? "" ]
        self.isUpdateEnabled = false
        viewModel.updatePlanStatus(dataDict: dataDict)
    }
    @objc func updateBtnClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
       let item = viewModel.planDetailsArr[indexPath!.section].product?[indexPath!.row]
        let dataDict : [String:Any] = ["row": "name" ,"p_id": item?.id ?? "","value" : item?.name ?? "" ]
        self.indexPath = nil
        viewModel.updatePlanStatus(dataDict: dataDict)
    }
    @objc func dropDownBtnClicked(sender:UIButton){
        let value = viewModel.planDetailsArr[sender.tag]
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Normal Benefits","Premiums"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            var dataDict : [String:Any] = ["row": "cat_status" ,"p_id": value.id ?? ""]
            
            if item == "Normal Benefits"{
                if value.cat_status != "0"{
                    dataDict["value"] = "0"
                }
            }else{
                if value.cat_status  != "1"{
                    dataDict["value"] = "1"
                }
            }
            viewModel.updatePlanStatus(dataDict: dataDict)
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
    @objc func bestInBtnClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let value = viewModel.planDetailsArr[indexPath!.section].product?[indexPath!.row]
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Low","High"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            var dataDict : [String:Any] = ["row": "status" ,"p_id": value?.id ?? ""]
            
            if item == "Low"{
                if value?.status != "0"{
                    dataDict["value"] = "0"
                }
            }else{
                if value?.status  != "1"{
                    dataDict["value"] = "1"
                }
            }
            viewModel.updatePlanStatus(dataDict: dataDict)
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
    @objc func categoryBtnClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        let value = viewModel.planDetailsArr[indexPath!.section].product?[indexPath!.row]
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Price","Age Range","Yes/No","Day Range","Month Range"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            var dataDict : [String:Any] = ["row": "cat_status" ,"p_id": value?.id ?? ""]
            
            if item == "Price"{
                if value?.cat_status != "0"{
                    dataDict["value"] = "0"
                }
            }else if item == "Age Range"{
                if value?.cat_status  != "1"{
                    dataDict["value"] = "1"
                }
            }else if item == "Yes/No"{
                if value?.cat_status  != "2"{
                    dataDict["value"] = "2"
                }
            }else if item == "Day Range"{
                if value?.cat_status  != "3"{
                    dataDict["value"] = "3"
                }
            }else{
                if value?.cat_status  != "4"{
                    dataDict["value"] = "4"
                }
            }
            viewModel.updatePlanStatus(dataDict: dataDict)
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
}

extension PlanViewController:UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        self.indexPath = indexPath
        self.tableView.reloadRows(at: [indexPath!], with: .none)
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        var value = textField.text
        value = value?.replacingOccurrences(of: "", with: "  ")
        viewModel.planDetailsArr[indexPath!.section].product?[indexPath!.row].name = value
        textField.resignFirstResponder()
        return true
    }
}
