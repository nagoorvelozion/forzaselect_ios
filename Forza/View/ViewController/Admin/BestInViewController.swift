//
//  BestInViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 16/04/21.
//

import UIKit
import Cosmos
class BestInViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var starView: CosmosView!
    var viewModel = OverviewViewModel()
    var id = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "BestInTableViewCell",bundle: nil), forCellReuseIdentifier: "BestInTableViewCell")
        bindviewModel()
    }
    func bindviewModel(){
        viewModel.getBestInResultList(id: self.id)
        viewModel.resultSuccess = { message , state in
            if state{
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        self.dismiss(animated: true, completion: nil)
    }
}

extension BestInViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.bestInArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "BestInTableViewCell", for: indexPath) as! BestInTableViewCell
        let item = viewModel.bestInArr[indexPath.row]
        cell.titleLbl.text = item.title ?? ""
        cell.subTitleLbl.text = item.sub_title ?? ""
        cell.starView.rating = Double(item.rating ?? 0)
        return cell
    }
    
    
}
