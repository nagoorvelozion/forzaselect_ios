//
//  AddPlansViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit
import DropDown
class AddPlansViewController: UIViewController {
    
    @IBOutlet weak var titleTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let dropDown = DropDown()
    var model = AddPlanModel()
    let viewModel = PlanCatagoriesViewModel()
    var onDismiss : ((String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AddPlanHeaderTableViewCell",bundle: nil), forCellReuseIdentifier: "AddPlanHeaderTableViewCell")
        tableView.register(UINib(nibName: "AddPlanTableViewCell",bundle: nil), forCellReuseIdentifier: "AddPlanTableViewCell")
        titleTF.delegate = self
        bindViewModel()
    }
    
    func bindViewModel(){
        viewModel.createPlanSuccess = { message in
            self.dismiss(animated: true, completion: {self.onDismiss?(message)})
            ProgressView.stopLoader()
        }
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func plusBtnClicked(_ sender: Any) {
        var obj = AddSubPlanModel()
        obj.benefits = nil
        obj.category = "0"
        obj.name = ""
        if self.model.subplans == nil{
            self.model.subplans = [AddSubPlanModel]()
        }
        self.model.subplans?.append(obj)
        self.tableView.reloadData()
    }
    
    @objc func subPlanAddBtnClicked(sender:UIButton){
        var benefit = AddPlanBenifitModel()
        benefit.category = "0"
        benefit.name = ""
        benefit.status = "0"
        if self.model.subplans?[sender.tag].benefits == nil{
            self.model.subplans?[sender.tag].benefits = [AddPlanBenifitModel]()
        }
        self.model.subplans?[sender.tag].benefits?.append(benefit)
        self.tableView.reloadData()
    }
    
    @objc func deleteBtnClicked(sender:UIButton){
        self.model.subplans?.remove(at: sender.tag)
        self.tableView.reloadData()
    }
    @objc func minusBtnClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        self.model.subplans?[indexPath!.section].benefits?.remove(at: indexPath!.row)
        self.tableView.reloadData()
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        if self.titleTF.text == ""{
            Utility.showAlertWith(message: "Please enter client name", title: "", forController: self)
            return
        }
        if self.model.subplans?.count == 0{
            Utility.showAlertWith(message: "Please add atlease one sub plan", title: "", forController: self)
            return
        }
        
        do{
            let jsonData = try self.model.jsonData()
            guard let jsonString = String(data: jsonData, encoding: .utf8) else {
                return
            }
            print("================================\n\(jsonString)\n===========================================")
            let dataDict : [String:Any] = ["data" : jsonString]
            viewModel.createNewPlans(dataDict: dataDict)
        }catch(let err){
            print(err.localizedDescription)
        }
    }
}

extension AddPlansViewController : UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return model.subplans?.count ?? 0
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return model.subplans?[section].benefits?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "AddPlanHeaderTableViewCell") as! AddPlanHeaderTableViewCell
        if model.subplans?[section].category == "0"{
            headerCell.textLbl.text = "Normal Benifits"
        }else{
            headerCell.textLbl.text = "Premiums"
        }
        headerCell.plusBtn.addTarget(self, action: #selector(subPlanAddBtnClicked), for: .touchUpInside)
        headerCell.minusBtn.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        headerCell.dropDownBtn.addTarget(self, action: #selector(subPlanCategoryDropDownClicked), for: .touchUpInside)
        headerCell.minusBtn.tag = section
        headerCell.plusBtn.tag = section
        headerCell.dropDownBtn.tag = section
        headerCell.titleTF.tag = section
        headerCell.titleTF.text = model.subplans?[section].name ?? ""
        headerCell.textFieldValue = { text, index in
            self.model.subplans?[index].name = text
            self.tableView.reloadData()
        }
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 100.0
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return CGFloat.leastNonzeroMagnitude
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddPlanTableViewCell", for: indexPath) as! AddPlanTableViewCell
        let item = model.subplans?[indexPath.section].benefits?[indexPath.row]
        cell.titleTF.text = item?.name ?? ""
        if item?.status == "0"{
            cell.statusLbl.text = "Low"
        }else{
            cell.statusLbl.text = "High"
        }
        switch item?.category {
        case "0":
            cell.categoryLbl.text = "Price"
        case "1":
            cell.categoryLbl.text = "Age Range"
        case "2":
            cell.categoryLbl.text = "Yes/No"
        case "3":
            cell.categoryLbl.text = "Day Range"
        default:
            cell.categoryLbl.text = "Month Range"
        }
        cell.minusBtn.addTarget(self, action: #selector(minusBtnClicked), for: .touchUpInside)
        cell.statusDropDownBtn.addTarget(self, action: #selector(statusDropDownClicked), for: .touchUpInside)
        cell.categoryDropDownBtn.addTarget(self, action: #selector(categoryDropDownClicked), for: .touchUpInside)
        cell.titleTF.delegate = self
        return cell
    }    
}

extension AddPlansViewController{
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    @objc func statusDropDownClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Low","High"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            if item == "Low"{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].status = "0"
            }else{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].status = "1"
            }
            self.dropDown.hide()
            self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        }
        self.dropDown.show()
    }
    @objc func categoryDropDownClicked(sender:UIButton){
        let buttonPosition = sender.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Price","Age Range","Yes/No","Day Range","Month Range"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            
            if item == "Price"{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].category = "0"
            }else if item == "Age Range"{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].category = "1"
            }else if item == "Yes/No"{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].category = "2"
            }else if item == "Day Range"{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].category = "3"
            }else{
                self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].category = "4"
            }
            self.dropDown.hide()
            self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        }
        self.dropDown.show()
        
        
    }
    @objc func subPlanCategoryDropDownClicked(sender:UIButton){
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Normal Benefits","Premiums"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            if item == "Normal Benefits"{
                self.model.subplans?[sender.tag].category = "0"
            }else{
                self.model.subplans?[sender.tag].category = "1"
            }
            self.dropDown.hide()
            self.tableView.reloadData()
        }
        self.dropDown.show()
    }
}
extension AddPlansViewController : UITextFieldDelegate{
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        return true
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == self.titleTF{
            self.model.planname = textField.text!
            textField.resignFirstResponder()
            return true
        }
        let buttonPosition = textField.convert(CGPoint.zero, to: self.tableView)
        let indexPath = self.tableView.indexPathForRow(at:buttonPosition)
        self.model.subplans?[indexPath!.section].benefits?[indexPath!.row].name = textField.text!
        textField.resignFirstResponder()
        self.tableView.reloadRows(at: [indexPath!], with: .automatic)
        return true
    }
}
