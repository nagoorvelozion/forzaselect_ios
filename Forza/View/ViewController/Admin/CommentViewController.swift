//
//  CommentViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class CommentViewController: UIViewController {
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var comentTF: UITextField!
    var viewModel = CommentViewModel()
    var planId = ""
    var carrierId = ""
    var name = ""
    var onDismiss : (()->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "CommentTableViewCell",bundle: nil), forCellReuseIdentifier: "CommentTableViewCell")
        bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.displayCommentAPI(planId: planId, carrierId: carrierId)
        self.titleLbl.text = "Comment Box (" + "\(name)" + ")"
    }
    func bindViewModel(){
        viewModel.commentSuccess = {
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.addCommentSuccess = { message in
            self.comentTF.text = ""
            Utility.showAlertWith(message: message, title: "", forController: self)
            self.viewModel.displayCommentAPI(planId: self.planId, carrierId: self.carrierId)
        }
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        comentTF.resignFirstResponder()
        if comentTF.text?.count != 0{
            let dataDict : [String:Any] = ["plan_id":planId,"career_id":carrierId,"comment":comentTF.text!]
            viewModel.addCommentAPI(dataDict: dataDict)
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: {self.onDismiss?()})
    }
}

extension CommentViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.commentArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CommentTableViewCell", for: indexPath) as! CommentTableViewCell
        let item = viewModel.commentArr[indexPath.row]
        cell.commentLbl.text = item.comment ?? ""
        cell.nameLbel.text = item.name ?? ""
        cell.dateLbl.text = item.date ?? ""
        var imageString = item.profile_img ?? ""
        if imageString.contains(" "){
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        }
        cell.profileImgVW.sd_setImage(with:URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        return cell
    }
}

extension CommentViewController:UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
