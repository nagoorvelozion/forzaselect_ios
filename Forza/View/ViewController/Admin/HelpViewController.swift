//
//  HelpViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class HelpViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel = HelpViewModel()
    var reason = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationType = .navPlain
        tableView.register(UINib(nibName: "HelpTableViewCell",bundle: nil), forCellReuseIdentifier: "HelpTableViewCell")
        tableView.register(UINib(nibName: "HelpNotesTableViewCell",bundle: nil), forCellReuseIdentifier: "HelpNotesTableViewCell")
        viewModel.apiCallForHelpList()
        bindViewModel()
    }
    func bindViewModel(){
        viewModel.success = { state,message in
            if state{
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
        viewModel.submitSuccess = { state, message in
            if state{
                self.reason = ""
                Utility.showAlertWith(message: message, title: "", forController: self)
                self.viewModel.apiCallForHelpList()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
                ProgressView.stopLoader()
            }
        }
    }
    @objc func submitBtnClicked(sender:UIButton){
        self.view.endEditing(true)
        if reason == ""{
            Utility.showAlertWith(message: "Please enter the message", title: "", forController: self)
            return
        }
        let removeSpaceString = self.reason.replacingOccurrences(of: " ", with: "%20")
        viewModel.apiCallForHelpSubmit(information: removeSpaceString)
    }
}

extension HelpViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 300.0
        }
        return 210.0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpTableViewCell", for: indexPath) as! HelpTableViewCell
            cell.reasonTW.text = reason
            cell.reasonTW.delegate = self
            cell.submitBtn.addTarget(self, action: #selector(submitBtnClicked), for: .touchUpInside)
            return cell
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "HelpNotesTableViewCell", for: indexPath) as! HelpNotesTableViewCell
            cell.valueTW.text = self.viewModel.helpArr.first?.message ?? ""
            return cell
        }
    }
    
}

extension HelpViewController:UITextViewDelegate{
    func textViewDidEndEditing(_ textView: UITextView) {
        self.reason = textView.text
        textView.resignFirstResponder()
    }
}
