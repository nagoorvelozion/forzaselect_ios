//
//  AssignViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import UIKit

class AssignViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel = CarrierListingViewModel()
    var id = ""
    var onDismiss : ((String)->())?
    var profile = [PlanOverviewProfileDetails]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "AssignCarrierTableViewCell",bundle: nil), forCellReuseIdentifier: "AssignCarrierTableViewCell")
        bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getCarrierList()
    }
    func bindViewModel(){
        viewModel.success = { state in
            for (index,item) in self.viewModel.carrierListArr.enumerated(){
                for obj in self.profile{
                    if item.id == obj.id{
                        self.viewModel.carrierListArr[index].isSelected = true
                    }
                }
            }
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.assignSuccess = { message in
            ProgressView.stopLoader()
            self.dismiss(animated: true, completion: {self.onDismiss?(message)})
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        let items = viewModel.carrierListArr.filter{$0.isSelected == true}
        if items.count > 0{
            var values = ""
            for item in items {
                if values == ""{
                    values = values + (item.id ?? "")
                }else{
                    values = values + "," + (item.id ?? "")
                }
            }
            viewModel.assignCarriers(p_id: self.id, carriers: values)
        }
    }
    
}
extension AssignViewController:UITableViewDataSource,UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.carrierListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AssignCarrierTableViewCell", for: indexPath) as! AssignCarrierTableViewCell
        let item = viewModel.carrierListArr[indexPath.row]
        cell.emailLbl.text = item.email ?? ""
        cell.nameLbl.text = item.name ?? ""
        var imageString = item.profile_img ?? ""
        if imageString.contains(" "){
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        }
        cell.imageVW.sd_setImage(with:URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        if item.isSelected == true{
            cell.selectionImage.isHidden = false
        }else{
            cell.selectionImage.isHidden = true
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if viewModel.carrierListArr[indexPath.row].isSelected == true{
            viewModel.carrierListArr[indexPath.row].isSelected = false
        }else{
            viewModel.carrierListArr[indexPath.row].isSelected = true
        }
       
        self.tableView.reloadData()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70.0
    }
}
