//
//  ViewresultsViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 17/04/21.
//

import UIKit

class ViewresultsViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var titleLbl: UILabel!
    var viewModel = OverviewViewModel()
    var id = ""
    var name = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ViewResultsListTableViewCell",bundle: nil), forCellReuseIdentifier: "ViewResultsListTableViewCell")
        tableView.register(UINib(nibName: "NewQuoteHeaderTableViewCell",bundle: nil), forCellReuseIdentifier: "NewQuoteHeaderTableViewCell")
        tableView.register(UINib(nibName: "ViewResultsTableViewCell",bundle: nil), forCellReuseIdentifier: "ViewResultsTableViewCell")
        self.titleLbl.text = "View Results (" + name + ")"
        bindviewModel()
    }
    func bindviewModel(){
        viewModel.getViewResultList(id: self.id)
        viewModel.resultSuccess = { message , state in
            if state{
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: message, title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension ViewresultsViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.resultsArr.count + 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            return 1
        }
        return viewModel.resultsArr[section - 1].sub?.count ?? 0
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0{
            return nil
        }
        let headerCell = tableView.dequeueReusableCell(withIdentifier: "NewQuoteHeaderTableViewCell") as! NewQuoteHeaderTableViewCell
        headerCell.titleLbl.text = viewModel.resultsArr[section - 1].name ?? ""
        return headerCell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0{
            return CGFloat.leastNonzeroMagnitude
        }
        return 40.0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "ViewResultsListTableViewCell", for: indexPath) as! ViewResultsListTableViewCell
            cell.dataArr = viewModel.plans.carrier ?? []
            return cell
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "ViewResultsTableViewCell", for: indexPath) as! ViewResultsTableViewCell
        let item = viewModel.resultsArr[indexPath.section - 1].sub?[indexPath.row]
        cell.subtitleLbl.text = item?.bic ?? ""
        cell.titleLbl.text = item?.benefit ?? ""
        cell.dataArr = item?.price ?? []
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            return 170.0
        }
        return UITableView.automaticDimension
    }
    
}
