//
//  ProfileViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet weak var lastLoginLbl: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var viewModel = ProfileViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationType = .navPlain
        tableView.register(UINib(nibName: "ProfileTableViewCell",bundle: nil), forCellReuseIdentifier: "ProfileTableViewCell")
        tableView.register(UINib(nibName: "ProfileImageTableViewCell",bundle: nil), forCellReuseIdentifier: "ProfileImageTableViewCell")
        bindViewModel()
        viewModel.getProfileDetails()
        lastLoginLbl.text = ""
    }
    func bindViewModel(){
        viewModel.success = { state in
            self.lastLoginLbl.text = "Last Edited Time : " + (self.viewModel.profileModel.details?.first?.edited_date ?? "")
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
    }
    @objc func editBtnClicked(sender:UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "EditProfileViewController") as! EditProfileViewController
        vc.profileModel = viewModel.profileModel.details?.first ?? ProfileDetailsModel()
        vc.onDismiss = { message in
            Utility.showAlertWith(message: message, title: "", forController: self)
            self.viewModel.getProfileDetails()
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
}

extension ProfileViewController:UITableViewDelegate,UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch section {
        case 0:
            return 1
        default:
            return 5
        }
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileImageTableViewCell", for: indexPath) as! ProfileImageTableViewCell
            var imageString = viewModel.profileModel.details?.first?.profile_img ?? ""
            if imageString.contains(" "){
                imageString = imageString.replacingOccurrences(of: " ", with: "%20")
            }
            cell.profileImg.sd_setImage(with: URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
            cell.editBtn.addTarget(self, action: #selector(editBtnClicked), for: .touchUpInside)
            return cell
            
        default:
            let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileTableViewCell", for: indexPath) as! ProfileTableViewCell
            switch indexPath.row {
            case 0:
                cell.contentLbl.text = viewModel.profileModel.details?.first?.name ?? ""
                cell.imageVw.image = UIImage(named: "name")
            case 1:
                cell.contentLbl.text = viewModel.profileModel.details?.first?.email ?? ""
                cell.imageVw.image = UIImage(named: "email")
            case 2:
                cell.contentLbl.text = viewModel.profileModel.details?.first?.phone ?? ""
                cell.imageVw.image = UIImage(named: "phone")
            case 3:
                cell.contentLbl.text = viewModel.profileModel.details?.first?.address ?? ""
                cell.imageVw.image = UIImage(named: "location")
            default:
                cell.contentLbl.text = "Logout"
                cell.imageVw.image = UIImage(named: "logout")
            }
            return cell
        }
        
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.section {
        case 0:
            return 180.0
        default:
            return 55.0
        }
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 1{
            if indexPath.row == 4{
                Utility.showAlertWithCompletion(message: "DO YOU WANT TO LOGOUT?", title: "", okTitle: "Yes", cancelTitle: "NO", forController: self) { (state) in
                    if state{
                        UserDefaultsManager.isUserLoggedIn = false
                        let story = UIStoryboard(name: "Main", bundle:nil)
                        let navigationController:UINavigationController = story.instantiateInitialViewController() as! UINavigationController
                        let statScreenVC = story.instantiateViewController(withIdentifier: "LoginViewController") as! LoginViewController
                        navigationController.viewControllers = [statScreenVC]
                        APP_DELEGATE.window?.rootViewController = navigationController
                        APP_DELEGATE.window?.makeKeyAndVisible()
                    }
                }
            }
        }
    }
}
