//
//  EditProfileViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 08/04/21.
//

import UIKit

class EditProfileViewController: UIViewController {

    @IBOutlet weak var addresTF: UITextField!
    @IBOutlet weak var mbTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    var profileModel = ProfileDetailsModel()
    var viewmodel = ProfileViewModel()
    var onDismiss : ((String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
        bindViewModel()
        updateUI()
    }
    func bindViewModel(){
        viewmodel.updateSuccess = { message in
            self.dismiss(animated: true, completion: {self.onDismiss?(message)})
        }
    }
    func updateUI(){
        nameTF.text = profileModel.name ?? ""
        emailTF.text = profileModel.email ?? ""
        mbTF.text = profileModel.phone ?? ""
        addresTF.text = profileModel.address ?? ""
    }
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        if nameTF.text == ""{
            Utility.showAlertWith(message: "Please enter name", title: "", forController: self)
        }else if mbTF.text == ""{
            Utility.showAlertWith(message: "Please enter mobile number", title: "", forController: self)
        }else if addresTF.text == ""{
            Utility.showAlertWith(message: "Please enter address", title: "", forController: self)
        }else{
            let dataDict : [String:Any] = ["name":nameTF.text!,"phone":mbTF.text!,"address":addresTF.text!]
            viewmodel.profileUpdate(dataDict: dataDict)
        }
    }
}
