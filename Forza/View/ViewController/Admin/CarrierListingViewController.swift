//
//  CarrierListingViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit
import DropDown
class CarrierListingViewController: BaseViewController {
    
    @IBOutlet weak var chooseFileTF: UITextField!
    @IBOutlet weak var passwordTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var userNameTF: UITextField!
    @IBOutlet weak var tableView: UITableView!
    var viewModel = CarrierListingViewModel()
    var imageString = ""
    let dropDown = DropDown()
    override func viewDidLoad() {
        super.viewDidLoad()
        customNavigationType = .navPlain
        tableView.register(UINib(nibName: "CarrierListingTableViewCell",bundle: nil), forCellReuseIdentifier: "CarrierListingTableViewCell")
        updateUI()
        bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getCarrierList()
    }
    func updateUI(){
        userNameTF.layer.borderWidth = 1.0
        userNameTF.layer.cornerRadius = 10.0
        userNameTF.layer.borderColor = UIColor(named: "PrimaryColor")?.cgColor
        emailTF.layer.borderWidth = 1.0
        emailTF.layer.cornerRadius = 10.0
        emailTF.layer.borderColor = UIColor(named: "PrimaryColor")?.cgColor
        passwordTF.layer.borderWidth = 1.0
        passwordTF.layer.cornerRadius = 10.0
        passwordTF.layer.borderColor = UIColor(named: "PrimaryColor")?.cgColor
        chooseFileTF.layer.borderWidth = 1.0
        chooseFileTF.layer.cornerRadius = 10.0
        chooseFileTF.layer.borderColor = UIColor(named: "PrimaryColor")?.cgColor
        chooseFileTF.backgroundColor = .systemGray4
    }
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    func bindViewModel(){
        viewModel.success = { state in
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.deleteSuccess = { message in
            Utility.showAlertWith(message: message, title: "", forController: self)
            self.viewModel.getCarrierList()
        }
    }
    @IBAction func submitBtnClicked(_ sender: Any) {
        if userNameTF.text?.count == 0{
            Utility.showAlertWith(message: "Please enter username", title: "", forController: self)
        }else if emailTF.text?.count == 0{
            Utility.showAlertWith(message: "Please enter email", title: "", forController: self)
        }else if passwordTF.text?.count == 0{
            Utility.showAlertWith(message: "Please enter password", title: "", forController: self)
        }else if imageString == ""{
            Utility.showAlertWith(message: "Please select profile image", title: "", forController: self)
        }else{
            let dataDict : [String:Any] = ["name" : userNameTF.text!,"email" : emailTF.text!,"password": passwordTF.text!,"image":imageString]
            viewModel.addCarrieritem(dataDict: dataDict)
        }
    }
    @objc func deleteBtnClicked(sender:UIButton){
        Utility.showAlertWithCompletion(message: "Are you sure to delete this carrier?", title: "", okTitle: "Yes", cancelTitle: "NO", forController: self) { (state) in
            if state{
                self.viewModel.deleteCarrieritem(id: self.viewModel.carrierListArr[sender.tag].id ?? "")
            }
        }
    }
    @objc func enableBtnClicked(sender:UIButton){
        customizeDropDown()
        self.dropDown.anchorView = sender
        self.dropDown.bottomOffset = CGPoint(x: 0, y: sender.bounds.height)
        let dataValues = ["Enabled","Disabled"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            print("Selected item: \(item) at index: \(index)")
            var dataDict : [String:Any] = ["p_id":viewModel.carrierListArr[sender.tag].id ?? "" ,"password":viewModel.carrierListArr[sender.tag].password ?? ""]
            
            if item == "Enabled"{
                if viewModel.carrierListArr[sender.tag].status != "0"{
                dataDict["status"] = "0"
                self.viewModel.setCarrierStatus(dataDict: dataDict)
                }
            }else{
                if viewModel.carrierListArr[sender.tag].status != "1"{

                dataDict["status"] = "1"
                self.viewModel.setCarrierStatus(dataDict: dataDict)
                }
            }
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
}

extension CarrierListingViewController : UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.carrierListArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CarrierListingTableViewCell", for: indexPath) as! CarrierListingTableViewCell
        let item = viewModel.carrierListArr[indexPath.row]
        cell.emailLbl.text = "Email \t : " + (item.email ?? "")
        cell.passwordLbl.text = "Password \t : " + (item.password ?? "")
        cell.userName.text = item.name ?? ""
        var imageString = item.profile_img ?? ""
        if imageString.contains(" "){
            imageString = imageString.replacingOccurrences(of: " ", with: "%20")
        }
        cell.imageVW.sd_setImage(with: URL(string:imageString), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        if item.status == "0"{
            cell.enableLbl.text = "Enable"
        }else{
            cell.enableLbl.text = "Disabled"
        }
        cell.enableBtn.tag = indexPath.row
        cell.deleteBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        cell.enableBtn.addTarget(self, action: #selector(enableBtnClicked), for: .touchUpInside)

        return cell
    }
}
extension CarrierListingViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //Show alert to selected the media source type.
    @objc private func showAlert() {
        let alert = UIAlertController(title: "Upload Photo", message: "From where you want to pick images?", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .camera)
        }))
        alert.addAction(UIAlertAction(title: "Photo Album", style: .default, handler: {(action: UIAlertAction) in
            self.getImage(fromSourceType: .photoLibrary)
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func getImage(fromSourceType sourceType: UIImagePickerController.SourceType) {
        if UIImagePickerController.isSourceTypeAvailable(sourceType) {
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            imagePickerController.sourceType = sourceType
            self.present(imagePickerController, animated: true, completion: nil)
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        self.dismiss(animated: true) { [weak self] in
            guard let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage else { return }
            let compressImgData = self?.compressImage(oldImage: image)
            let base64 = (compressImgData?.base64EncodedString(options: .init(rawValue: 0)))!
            self?.imageString = base64.trimmingCharacters(in: .whitespacesAndNewlines)
            self?.imageString = (self?.imageString.addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed))!
            self?.chooseFileTF.text = "file"
        }
    }
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    func compressImage(oldImage:UIImage) -> Data {
        var imageData =  Data(oldImage.pngData()! )
        print("***** Uncompressed Size \(imageData.description) **** ")
        imageData = oldImage.jpegData(compressionQuality: 1.0)!
        print("***** Compressed Size \(imageData.description) **** ")
        return imageData
    }
}
extension CarrierListingViewController:UITextFieldDelegate{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == chooseFileTF{
            textField.resignFirstResponder()
            self.showAlert()
        }
    }
}
extension CharacterSet {
    
    /// Character set containing characters allowed in query value as outlined in RFC 3986.
    ///
    /// RFC 3986 states that the following characters are "reserved" characters.
    ///
    /// - General Delimiters: ":", "#", "[", "]", "@", "?", "/"
    /// - Sub-Delimiters: "!", "$", "&", "'", "(", ")", "*", "+", ",", ";", "="
    ///
    /// In RFC 3986 - Section 3.4, it states that the "?" and "/" characters should not be escaped to allow
    /// query strings to include a URL. Therefore, all "reserved" characters with the exception of "?" and "/"
    /// should be percent-escaped in the query string.
    ///
    /// - parameter string: The string to be percent-escaped.
    ///
    /// - returns: The percent-escaped string.
    
    static var urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: generalDelimitersToEncode + subDelimitersToEncode)
        
        return allowed
    }()
    
}
