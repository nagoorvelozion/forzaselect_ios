//
//  NotificationViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import UIKit

class NotificationViewController: BaseViewController {

    @IBOutlet weak var tableView: UITableView!
    var viewModel = NotificationViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.customNavigationType = .navWithBack
        tableView.register(UINib(nibName: "NotificationTableViewCell",bundle: nil), forCellReuseIdentifier: "NotificationTableViewCell")
        
        ProgressView.startLoader("")
        viewModel.getNotificationList()
        bindViewModel()
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.tabBarController?.tabBar.isHidden = false

    }

    func bindViewModel(){
        viewModel.notificationSuccess = { count,state in
            self.tableView.reloadData()
            ProgressView.stopLoader()
        }
        viewModel.success = { message,state in
            let alertDisapperTimeInSeconds = 2.0
            let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
            alert.view.layer.cornerRadius = 20
            if state{
                alert.view.backgroundColor = .systemGreen
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
                  alert.dismiss(animated: true)
                }
                self.present(alert, animated: true)
            }else{
                alert.view.backgroundColor = .red
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
                  alert.dismiss(animated: true)
                }
                self.present(alert, animated: true)
            }
            ProgressView.stopLoader()
            self.viewModel.getNotificationList()
        }
    }
    override func selectedBack(sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
}

extension NotificationViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.notificationData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "NotificationTableViewCell", for: indexPath) as! NotificationTableViewCell
        let item = viewModel.notificationData[indexPath.row]
        cell.dateLbl.text = item.date ?? ""
        cell.contentLbl.text = item.content ?? ""
        cell.imageVw.sd_setImage(with:URL(string:item.profile_img ?? ""), placeholderImage: UIImage(named: "logo_icon"), options: .allowInvalidSSLCertificates, context: nil)
        if item.seen == "0"{
            cell.statusImageVw.image = UIImage(named: "dot1")
        }else{
            cell.statusImageVw.image = UIImage(named: "dot2")
        }
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.viewModel.makeNotificationRead(id: viewModel.notificationData[indexPath.row].notf_id ?? "")
    }
}
