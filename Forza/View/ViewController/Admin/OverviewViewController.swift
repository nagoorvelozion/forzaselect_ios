//
//  OverviewViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import UIKit

class OverviewViewController: BaseViewController {
    
    @IBOutlet weak var tableView: UITableView!
    var viewModel = OverviewViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "OverviewTableViewCell",bundle: nil), forCellReuseIdentifier: "OverviewTableViewCell")
        
        bindViewModel()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        viewModel.getNotificationList()
        viewModel.getPlanOverviewList()
    }
    func bindViewModel(){
        viewModel.success = { state in
            if state {
                self.tableView.reloadData()
            }else{
                Utility.showAlertWith(message: "No Records Found", title: "", forController: self)
            }
            ProgressView.stopLoader()
        }
        viewModel.notificationSuccess = { count in
            UserDefaultsManager.notificationCount = count
            self.customNavigationType = .navPlain
        }
        viewModel.deletePlanSuccess = { message in
            Utility.showAlertWith(message: "No Records Found", title: "", forController: self)
        }
    }
    @objc func assignBtnClicked(sender:UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "AssignViewController") as! AssignViewController
        vc.id = viewModel.dataArr[sender.tag].id ?? ""
        vc.profile = viewModel.dataArr[sender.tag].profile ?? []
        vc.onDismiss = { message in
            Utility.showAlertWith(message: message, title: "", forController: self)
            self.viewModel.getNotificationList()
            self.viewModel.getPlanOverviewList()
        }
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    @objc func deleteBtnClicked(sender:UIButton){
        Utility.showAlertWithCompletion(message: "Are you sure, want to delete?", title: "", okTitle: "Yes", cancelTitle: "NO", forController: self) { (state) in
            if state{
                self.viewModel.deletePlan(id: self.viewModel.dataArr[sender.tag].id ?? "")
            }
        }
    }
    @objc func bestInViewBtnClicked(sender:UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "BestInViewController") as! BestInViewController
        vc.id = viewModel.dataArr[sender.tag].id ?? ""
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
    @objc func viewResultBtnClicked(sender:UIButton){
        let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ViewresultsViewController") as! ViewresultsViewController
        vc.id = viewModel.dataArr[sender.tag].id ?? ""
        vc.name = viewModel.dataArr[sender.tag].name ?? ""
        vc.modalPresentationStyle = .overCurrentContext
        vc.modalTransitionStyle = .crossDissolve
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.tabBarController?.present(vc, animated: true, completion: nil)
    }
}

extension OverviewViewController:UITableViewDelegate,UITableViewDataSource{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataArr.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OverviewTableViewCell", for: indexPath) as! OverviewTableViewCell
        let item = viewModel.dataArr[indexPath.row]
        cell.titleLbl.text = item.name ?? ""
        cell.dateLbl.text = item.created_date ?? ""
        cell.statusBtn.setTitle(("  " + (item.status ?? "") + "  "), for: .normal)
        if item.status == "Active"{
            cell.viewBestInClassBtn.isHidden = false
            cell.viewResultBtn.isHidden = false
            cell.statusBtn.backgroundColor = #colorLiteral(red: 0, green: 0.6729254723, blue: 0.4588291049, alpha: 1)
        }else{
            cell.viewBestInClassBtn.isHidden = true
            cell.viewResultBtn.isHidden = true
            cell.statusBtn.backgroundColor = #colorLiteral(red: 0, green: 0.6261201501, blue: 0.952124536, alpha: 1)
        }
        if item.profile?.count == 0{
            cell.collectionVWHtConst.constant = 0.0
            cell.cLblHtConst.constant = 0.0
        }else{
            cell.collectionVWHtConst.constant = 40.0
            cell.cLblHtConst.constant = 16.0
        }
        cell.dataArr = item.profile ?? []
        cell.commentBtnClicked = { data in
            let storyboard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let vc = storyboard.instantiateViewController(withIdentifier: "CommentViewController") as! CommentViewController
            vc.carrierId = data.id ?? ""
            vc.planId = item.id ?? ""
            vc.name = item.name?.capitalized ?? ""
            vc.onDismiss = {
                self.viewModel.getPlanOverviewList()
            }
            vc.modalPresentationStyle = .overCurrentContext
            vc.modalTransitionStyle = .crossDissolve
            vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            self.tabBarController?.present(vc, animated: true, completion: nil)
        }
        cell.assignBtn.addTarget(self, action: #selector(assignBtnClicked), for: .touchUpInside)
        cell.assignBtn.tag = indexPath.row
        cell.deleteBtn.addTarget(self, action: #selector(deleteBtnClicked), for: .touchUpInside)
        cell.deleteBtn.tag = indexPath.row
        cell.viewBestInClassBtn.addTarget(self, action: #selector(bestInViewBtnClicked), for: .touchUpInside)
        cell.viewBestInClassBtn.tag = indexPath.row
        cell.viewResultBtn.addTarget(self, action: #selector(viewResultBtnClicked), for: .touchUpInside)
        cell.viewResultBtn.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if viewModel.dataArr[indexPath.row].profile?.count == 0{
            return 115.0
        }else{
            return 170.0
        }
    }
}
