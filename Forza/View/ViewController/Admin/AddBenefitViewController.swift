//
//  AddBenefitViewController.swift
//  Forza
//
//  Created by Naveen Kumar K N on 08/04/21.
//

import UIKit
import DropDown

class AddBenefitViewController: UIViewController {
    
    @IBOutlet weak var statusTF: UITextField!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var subPlanTF: UITextField!
    var planDetailsArr = [PlanHeadingModel]()
    var subPlanArr = [String]()
    var id = ""
    let dropDown = DropDown()
    var onDismiss : ((String)->())?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func customizeDropDown() {
        DropDown.appearance().cellHeight = 40
        DropDown.appearance().backgroundColor = UIColor.white
        DropDown.appearance().selectionBackgroundColor = UIColor.lightGray
        DropDown.appearance().shadowOpacity = 0.9
        DropDown.appearance().shadowRadius = 0
        DropDown.appearance().animationduration = 0.25
    }
    
    @IBAction func cancelBtnClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func subPlanSelectionBtn(_ sender: Any) {
        let btn = sender as! UIButton
        customizeDropDown()
        self.dropDown.anchorView = btn
        self.dropDown.bottomOffset = CGPoint(x: 0, y: btn.bounds.height)
        dropDown.dataSource = self.subPlanArr
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.subPlanTF.text = item
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
    
    @IBAction func statusSelectionBtnClicked(_ sender: Any) {
        let btn = sender as! UIButton
        customizeDropDown()
        self.dropDown.anchorView = btn
        self.dropDown.bottomOffset = CGPoint(x: 0, y: btn.bounds.height)
        let dataValues = ["Low","High"]
        dropDown.dataSource = dataValues
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.statusTF.text = item
            self.dropDown.hide()
        }
        self.dropDown.show()
    }
    
    @IBAction func submitBtnClicked(_ sender: UIButton) {
        if subPlanTF.text == "" || subPlanTF.text == "Select sub plan"{
            Utility.showAlertWith(message: "Please select sub plan", title: "", forController: self)
        }else if nameTF.text == ""{
            Utility.showAlertWith(message: "Please enter name", title: "", forController: self)
        }else if statusTF.text == ""{
            Utility.showAlertWith(message: "Please select status", title: "", forController: self)
        }else{
            let item = self.planDetailsArr.filter{$0.name == subPlanTF.text}
            let dataDict : [String:Any] = ["name":nameTF.text ?? "","p_id" : self.id,"category_id":item.first?.category_id ?? "","status":statusTF.text == "Low" ? "0" : "1"]
            self.addNewPlan(dataDict: dataDict)
        }
    }
}
extension AddBenefitViewController{
    func addNewPlan(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.addNewPlans(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                ProgressView.stopLoader()
                self.dismiss(animated: true, completion: {self.onDismiss?(model.message ?? "")})
            case .failure(let err):
                ProgressView.stopLoader()
                self.dismiss(animated: true, completion: {self.onDismiss?(err.localizedDescription)})
            }
            
        }
    }
}
