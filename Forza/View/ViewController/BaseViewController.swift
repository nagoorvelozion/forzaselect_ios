//
//  BaseViewController.swift
//  Rally
//
//  Created by Naveen Kumar K N on 20/11/20.
//

import UIKit

class BaseViewController: UIViewController {

    

    var navigationType  : CustomNavType           = .navPlain
    
    var customNavigationType : CustomNavType {
        
        get {
            return self.navigationType
        }
        set (customNavigationType) {
            self.navigationType = customNavigationType
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.font: UIFont.getFont(with: .RobotoBold, size: 20)]
            navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.white]
            switch customNavigationType {
            case .navWithBack:
                let logo = UIImage(named: "logo.png")
                let imageView = UIImageView(image:logo)
                    imageView.contentMode = .scaleAspectFit
                navigationItem.titleView = imageView
                navigationItem.titleView?.contentMode = .scaleAspectFit
                let backBtn =  NavBackButton(type: .custom)
                backBtn.addTarget(self, action: #selector(selectedBack(sender:)), for: .touchUpInside)
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backBtn)
                navigationItem.rightBarButtonItem = nil
                break
            case .navWithBellandLogout:
                let logo = UIImage(named: "logo.png")
                let imageView = UIImageView(image:logo)
                    imageView.contentMode = .scaleAspectFit
                navigationItem.titleView = imageView
                let logoutBtn =  NavLogoutButton(type: .custom)
                logoutBtn.addTarget(self, action: #selector(logoutSelected(sender:)), for: .touchUpInside)
                navigationItem.rightBarButtonItems = [ UIBarButtonItem(customView:logoutBtn)]
                let notificationButton = SSBadgeButton()
                notificationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                notificationButton.setImage(UIImage(named: "notification"), for: .normal)
                notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 25)
                notificationButton.semanticContentAttribute = .forceLeftToRight
                notificationButton.badge = UserDefaultsManager.notificationCount
                notificationButton.addTarget(self, action: #selector(notificationSelected(sender:)), for: .touchUpInside)
                navigationItem.leftBarButtonItems = [ UIBarButtonItem(customView:notificationButton)]
                break
            case .navWithBackandEdit:
                let backBtn =  NavBackButton(type: .custom)
                backBtn.addTarget(self, action: #selector(selectedBack(sender:)), for: .touchUpInside)
                navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backBtn)
                let doneBtn =  NavEditButton(type: .custom)
                doneBtn.addTarget(self, action: #selector(editSelected(sender:)), for: .touchUpInside)
                navigationItem.rightBarButtonItem = UIBarButtonItem(customView: doneBtn)
                break
            case .navPlain:
                let logo = UIImage(named: "logo.png")
                let imageView = UIImageView(image:logo)
                    imageView.contentMode = .scaleAspectFit
                navigationItem.titleView = imageView
                navigationItem.titleView?.contentMode = .scaleAspectFit
                self.navigationItem.setHidesBackButton(true, animated:true)
                navigationItem.leftBarButtonItem = nil
                let notificationButton = SSBadgeButton()
                notificationButton.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
                notificationButton.setImage(UIImage(named: "notification"), for: .normal)
                notificationButton.badgeEdgeInsets = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
                notificationButton.badge = UserDefaultsManager.notificationCount
                notificationButton.addTarget(self, action: #selector(notificationSelected(sender:)), for: .touchUpInside)
                navigationItem.rightBarButtonItems = [ UIBarButtonItem(customView:notificationButton)]
                break
            }
        }
    }    

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    func setNavBarTransparent() {
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    func setNavBarWithColor(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.view.backgroundColor = UIColor.clear
    }
    
    @objc func selectedBack(sender: UIButton) {
        print("Overide")
    }
    
    @objc func menuSelected(sender: UIButton) {
        print("Overide")
    }
    
    @objc func editSelected(sender: UIButton) {
        print("Overide")
    }
    
    @objc func plusSelected(sender: UIButton) {
        print("Overide")
    }
    
    @objc func powerSelected(sender: UIButton) {
        print("Overide")
    }
    @objc func logoutSelected(sender: UIButton) {
        print("Overide")
    }
    @objc func notificationSelected(sender: UIButton) {
        let storyBoard = UIStoryboard(name: "Admin", bundle: nil)
        let VC = storyBoard.instantiateViewController(withIdentifier: "NotificationViewController") as? NotificationViewController
        self.tabBarController?.tabBar.isHidden = true
        self.navigationController?.pushViewController(VC!, animated: true)
    }
}
