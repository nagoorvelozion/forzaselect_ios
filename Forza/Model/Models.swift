//
//  Models.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import Foundation

//MARK:- General Model
struct MessageModel:Decodable {
    var status : Int?
    var message : String?
    var details : String?
}
//MARK:- Login Model
struct LoginModel:Decodable {
    var status : Int?
    var message : String?
    var details : LoginModelDetails?
}
struct LoginModelDetails:Decodable {
    var id : String?
    var email : String?
    var access : String?
    var status : String?
    var access_priority : String?
    var admin_id : String?
    var name : String?
}

//MARK:- Plan Overview Model
struct PlanOverviewModel:Decodable {
    var status : Int?
    var message : String?
    var details : [PlanOverviewDetails]?
}
struct PlanOverviewDetails:Decodable {
    var id : String?
    var parent_id : String?
    var carrier : String?
    var status : String?
    var category_id : String?
    var edited_date : String?
    var name : String?
    var cat_status : String?
    var created_date : String?
    var order_status : String?
    var noti_status : String?
    var admin_id : String?
    var count : [CountDetails]?
    var profile : [PlanOverviewProfileDetails]?
}
struct CountDetails:Decodable {
    var count : String?
}
struct PlanOverviewProfileDetails:Decodable {
    var profile_img : String?
    var name : String?
    var id : String?
    var count : String?
}

//MARK:- Notification Model
struct NotificationModel:Decodable {
    var status : Int?
    var message : String?
    var details : [NotificationDetails]?
    var notf_ct : NotificationCoutModel?
}
struct NotificationCoutModel:Decodable {
    var count : String?
}
struct NotificationDetails:Decodable {
    var notf_id : String?
    var content : String?
    var noti_from : String?
    var user : String?
    var name : String?
    var plan_id : String?
    var date : String?
    var seen : String?
    var noti_cat : String?
    var profile_img : String?
    var noti_status : String?
}

//MARK:- Profile Model

struct ProfileModel:Decodable {
    var status : Int?
    var message : String?
    var details : [ProfileDetailsModel]?
}
struct ProfileDetailsModel:Decodable {
    var id : String?
    var name : String?
    var count : String?
    var email : String?
    var phone : String?
    var address : String?
    var profile_img : String?
    var edited_date : String?
}

//MARK:- Carrier List Model
struct CarrierListModel:Decodable {
    var status : Int?
    var message : String?
    var details : [CarrierListDetailsModel]?
}
struct CarrierListDetailsModel:Decodable {
    var id : String?
    var name : String?
    var email : String?
    var password : String?
    var status : String?
    var profile_img : String?
    var isSelected : Bool?
}

//MARK:- Plan Catagories Model
struct PlanCatagoriesModel:Decodable {
    var status : Int?
    var message : String?
    var details : [PlanCatagoriesDetailsModel]?
}
struct PlanCatagoriesDetailsModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var isSelected : Bool?
}

//MARK:- Plan Details Model
struct PlanDetailsModel:Decodable {
    var status : Int?
    var message : String?
    var details : [PlanCategoryDetailsModel]?
}
struct PlanCategoryDetailsModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var parent_id : String?
    var carrier : String?
    var heading :  [PlanHeadingModel]?
}

struct PlanHeadingModel:Decodable {
    var cat_status : String?
    var id : String?
    var name : String?
    var category_id : String?
    var parent_id : String?
    var product : [PlanProductModel]?
}
struct PlanProductModel:Decodable {
    var status : String?
    var cat_status : String?
    var id : String?
    var name : String?
    var category_id : String?
    var parent_id : String?
}

//MARK:-  Comment Model
struct CommentModel:Decodable {
    var status : Int?
    var message : String?
    var details : [CommentDetailsModel]?
}
struct CommentDetailsModel:Decodable {
    var comment : String?
    var name : String?
    var profile_img : String?
    var user_id : String?
    var date : String?
}
//MARK:-  Best In Result Model
struct BestInModel:Decodable {
    var status : Int?
    var message : String?
    var details : [BestInDetailsModel]?
}
struct BestInDetailsModel:Decodable {
    var title : String?
    var rating : Int?
    var sub_title : String?
}
//MARK:-  View Result Model
struct ViewResultsModel:Decodable {
    var status : Int?
    var message : String?
    var details : [ViewResultsDetailsModel]?
    var plans : ViewResultsPlansModel?
}
struct ViewResultsDetailsModel:Decodable {
    var name : String?
    var sub : [ViewResultsSubModel]?
}
struct ViewResultsSubModel:Decodable {
    var id : String?
    var benefit : String?
    var bic : String?
    var price : [ViewResultsPriceModel]?
}
struct ViewResultsPriceModel:Decodable {
    var value : String?
    var isbest : Int?
}
struct ViewResultsPlansModel:Decodable {
    var plan_id : String?
    var plan_name : String?
    var carrier : [ViewResultsCarrierModel]?
}
struct ViewResultsCarrierModel:Decodable {
    var id : String?
    var name : String?
    var profile_img : String?
}


//MARK:- Add New Plan Model
struct AddPlanModel:Encodable {
    var planname : String?
    var subplans : [AddSubPlanModel]?
}

struct AddSubPlanModel:Encodable {
    var name : String?
    var category : String?
    var benefits : [AddPlanBenifitModel]?
}
struct AddPlanBenifitModel:Encodable {
    var name : String?
    var category : String?
    var status : String?
}
//MARK:- Quotation Model
struct QuotationModel:Decodable {
    var status : Int?
    var message : String?
    var details : [QuotationDetailsModel]?
}
struct QuotationDetailsModel:Decodable {
    var id : String?
    var name : String?
    var carrier : String?
    var category_id : String?
    var edited_date : String?
    var noti_status : String?
    var date : String?
    var time : String?
    var status : String?
    var products : [QuotationProductModel]?
    var count : String?
    var user_id : String?
    var noti : Int?
}
struct QuotationProductModel:Decodable {
    var product_id : String?
    var product_name : String?
}

//MARK:-  Quotation New Quote Model
struct NewQuoteModel:Decodable {
    var status : Int?
    var message : String?
    var details : [NewQuoteDetailsModel]?
}
struct NewQuoteDetailsModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var subject : [NewQuoteSubjectModel]?
}
struct NewQuoteSubjectModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var parent_id : String?
    var products : [NewQuoteProductModel]?
}

struct NewQuoteProductModel:Decodable {
    var id : String?
    var name : String?
    var cat_status : String?
    var parent_id : String?
    var value : String?
}

//MARK:-  Quotation Edit Quote Model
struct EditQuoteModel:Decodable {
    var status : Int?
    var message : String?
    var details : [EditQuoteDetailsModel]?
}
struct EditQuoteDetailsModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var parent_id : String?
    var products : [EditQuoteProductModel]?
}

struct EditQuoteProductModel:Decodable {
    var id : String?
    var name : String?
    var category_id : String?
    var cat_status : String?
    var parent_id : String?
    var cost_price : String?
    var cost_id : String?
}

struct NewQuoteSubmitModel:Codable {
    var data : [NewQuoteProductSubmitModel]?
}
struct NewQuoteProductSubmitModel:Codable {
    var products_id : String?
    var name : String?
    var price : String?
}

//MARK:-  Help Model
struct HelpModel:Decodable {
    var status : Int?
    var message : String?
    var details : [HelpDetailsModel]?
}
struct HelpDetailsModel:Decodable {
    var id : String?
    var message : String?
}
