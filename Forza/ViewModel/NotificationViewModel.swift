//
//  NotificationViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import Foundation

class NotificationViewModel:NSObject{
    var notificationData = [NotificationDetails]()
    var notificationSuccess : ((String,Bool)->())?
    var success : ((String,Bool)->())?
}
extension NotificationViewModel{
    func getNotificationList(){
        NetworkClient.getNotificationList(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.notificationData = model.details ?? []
                    self.notificationSuccess?(model.notf_ct?.count ?? "0",true)
                }else{
                    self.notificationSuccess?("0",false)
                }
            case .failure(let err):
                print(err)
                self.notificationSuccess?("0",false)
            }
        }
    }
    func makeNotificationRead(id:String){
        ProgressView.startLoader("")
        NetworkClient.makeNotificationRead(id: id){ result in
            switch result{
            case .success(let model):
                self.success?(model.message ?? "",model.status == 1 ? true : false)
            case .failure(let err):
                self.success?(err.localizedDescription,false)
                print(err)
            }
            
        }
    }

}
