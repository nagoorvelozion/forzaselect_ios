//
//  LoginViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import Foundation

class LoginViewModel: NSObject{
    var loginSuccess : ((Bool)->())?
    var carrierLoginSuccess : (()->())?
}

extension LoginViewModel{
    func callLoginAPI(userName:String,pwd:String){
        ProgressView.startLoader("")
        NetworkClient.loginAPICall(userName:userName,pwd:pwd){ result in
            switch result{
            case .success(let model):
                print(model)
                if model.status == 1{
                    UserDefaultsManager.userId = userName
                    UserDefaultsManager.pwd = pwd
                    if model.details?.access == "2"{
                        self.carrierLoginSuccess?()
                        return
                    }
                    self.loginSuccess?(true)
                }else{
                    self.loginSuccess?(false)
                }
                ProgressView.stopLoader()
            case .failure(let err):
                print(err)
                self.loginSuccess?(false)
            }
        }
    }
}
