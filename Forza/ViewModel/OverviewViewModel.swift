//
//  OverviewViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import Foundation

class OverviewViewModel: NSObject {
    var dataArr = [PlanOverviewDetails]()
    var bestInArr = [BestInDetailsModel]()
    var resultsArr = [ViewResultsDetailsModel]()
    var plans = ViewResultsPlansModel()
    var success : ((Bool)->())?
    var notificationSuccess : ((String)->())?
    var deletePlanSuccess : ((String)->())?
    var resultSuccess : ((String,Bool)->())?
}
extension OverviewViewModel{
    func getPlanOverviewList(){
        ProgressView.startLoader("")
        NetworkClient.getOverviewList(){result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.dataArr = model.details ?? []
                    self.success?(true)
                }else{
                    self.success?(false)
                }
            case .failure(let err):
                print(err)
                self.success?(false)
            }
            
        }
    }
    func getNotificationList(){
        NetworkClient.getNotificationList(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.notificationSuccess?(model.notf_ct?.count ?? "0")
                }else{
                    self.notificationSuccess?("0")
                }
            case .failure(let err):
                print(err)
                self.notificationSuccess?("0")
            }
        }
    }
    func deletePlan(id:String){
        ProgressView.startLoader("")
        NetworkClient.deleteCarrierPlan(id: id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.getPlanOverviewList()
                    self.deletePlanSuccess?(model.message ?? "")
                }else{
                    self.deletePlanSuccess?(model.message ?? "")
                    ProgressView.stopLoader()
                }
            case .failure(let err):
                self.deletePlanSuccess?(err.localizedDescription)
                ProgressView.stopLoader()
            }
        }
    }
    func getBestInResultList(id:String){
        ProgressView.startLoader("")
        NetworkClient.viewBestInResults(id: id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.bestInArr = model.details ?? []
                    self.resultSuccess?(model.message ?? "",true)
                }else{
                    self.resultSuccess?(model.message ?? "",false)
                }
            case .failure(let err):
                self.resultSuccess?(err.localizedDescription,false)            }
        }
    }
    func getViewResultList(id:String){
        ProgressView.startLoader("")
        NetworkClient.viewResults(id: id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.plans = model.plans ?? ViewResultsPlansModel()
                    self.resultsArr = model.details ?? []
                    self.resultSuccess?(model.message ?? "",true)
                }else{
                    self.resultSuccess?(model.message ?? "",false)
                }
            case .failure(let err):
                self.resultSuccess?(err.localizedDescription,false)
            }
        }
    }

}
