//
//  ProfileViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import Foundation

class ProfileViewModel:NSObject{
    var success : ((Bool)->())?
    var updateSuccess : ((String)->())?
    var profileModel = ProfileModel()
}
extension ProfileViewModel{
    func getProfileDetails(){
        ProgressView.startLoader("")
        NetworkClient.getProfileDetails(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.profileModel = model 
                    self.success?(true)
                }else{
                    self.success?(false)
                }
            case .failure(let err):
                print(err)
                self.success?(false)
            }
        }
    }
    func profileUpdate(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.editProfile(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.updateSuccess?(model.details ?? "")
                }else{
                    self.updateSuccess?(model.details ?? "")
                }
            case .failure(let err):
                self.updateSuccess?(err.localizedDescription)
            }
        }
    }
}
