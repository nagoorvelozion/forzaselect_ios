//
//  CarrierListingViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import Foundation

class CarrierListingViewModel:NSObject{
    var carrierListArr = [CarrierListDetailsModel]()
    var success : ((Bool)->())?
    var assignSuccess : ((String)->())?
    var deleteSuccess : ((String)->())?
}

extension CarrierListingViewModel{
    func getCarrierList(){
        ProgressView.startLoader("")
        NetworkClient.getCarrierList(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                self.carrierListArr = model.details ?? []
                    self.success?(true)
                }else{
                    self.success?(false)
                }
            case .failure(let err):
                print(err)
                self.success?(false)
            }
            
        }
    }
    func assignCarriers(p_id:String,carriers:String){
        ProgressView.startLoader("")
        NetworkClient.assignCarriers(planId: p_id, CarrierId: carriers){ result in
            switch result{
            case .success(let model):
                self.assignSuccess?(model.message ?? "")
            case .failure(let err):
                self.assignSuccess?(err.localizedDescription)
            }
        }
    }
    func deleteCarrieritem(id:String){
        ProgressView.startLoader("")
        NetworkClient.deleteCarrierListItem(id: id){ result in
            switch result{
            case .success(let model):
                self.deleteSuccess?(model.message ?? "")
            case .failure(let err):
                self.deleteSuccess?(err.localizedDescription)
            }
        }
    }
    func addCarrieritem(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.addCarrierItem(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                print(model)
                self.deleteSuccess?(model.message ?? "")

            case .failure(let err):
                print(err)
                self.deleteSuccess?(err.localizedDescription)
            }
        }
    }
    func setCarrierStatus(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.setCarrierStatus(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                print(model)
                self.deleteSuccess?(model.message ?? "")
            case .failure(let err):
                print(err)
                self.deleteSuccess?(err.localizedDescription)
            }
        }
    }
}
