//
//  QuotationsViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 09/04/21.
//

import Foundation

class QuotationsViewModel:NSObject{
    var success : ((String,Bool)->())?
    var submitQuoteSuccess : ((String,Bool)->())?

    var notificationSuccess : ((String)->())?
    var quotationArr = [QuotationDetailsModel]()
    var newQuoteArr = [NewQuoteSubjectModel]()
}

extension QuotationsViewModel{
    func getNotificationList(){
        NetworkClient.getNotificationList(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.notificationSuccess?(model.notf_ct?.count ?? "0")
                }else{
                    self.notificationSuccess?("0")
                }
            case .failure(let err):
                print(err)
                self.notificationSuccess?("0")
            }
        }
    }
    func getQuotationsList(){
        ProgressView.startLoader("")
        NetworkClient.getQuotationList(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.quotationArr = model.details ?? []
                    self.success?(model.message ?? "" , true)
                }else{
                    self.success?(model.message ?? "" , false)
                }
            case .failure(let err):
                self.success?(err.localizedDescription , false)
            }
        }
    }
    func getNewQuotesParameters(id:String){
        ProgressView.startLoader("")
        NetworkClient.getNewQuoteParameters(id: id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.newQuoteArr = model.details?.first?.subject ?? []
                    self.success?(model.message ?? "" , true)
                }else{
                    self.success?(model.message ?? "" , false)
                }
            case .failure(let err):
                print(err)
            }
        }
    }
    func submitNewQuotesParameters(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.submitQuote(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.submitQuoteSuccess?(model.message ?? "" , true)
                }else{
                    self.submitQuoteSuccess?(model.message ?? "" , false)
                }
            case .failure(let err):
                self.submitQuoteSuccess?(err.localizedDescription , false)
            }
        }
    }

}
