//
//  PlanCatagoriesViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 05/04/21.
//

import Foundation

class PlanCatagoriesViewModel:NSObject{
    var categoryListArr = [PlanCatagoriesDetailsModel]()
    var planDetailsArr = [PlanHeadingModel]()
    var success : ((Bool)->())?
    var planDetailSuccess : ((Bool)->())?
    var updateSuccess : ((String)->())?
    var createPlanSuccess : ((String)->())?
    var id = ""
}
extension PlanCatagoriesViewModel{
    func getAllPlanCatagories(){
        ProgressView.startLoader("")
        NetworkClient.getPlanCatagories(){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.categoryListArr = model.details ?? []
                    self.categoryListArr[0].isSelected = true
                    self.id = self.categoryListArr[0].id ?? ""
                    self.getPlanDetails(id: self.categoryListArr[0].id ?? "")
                    self.success?(true)
                }else{
                    self.success?(false)
                }
            case .failure(let err):
                print(err)
                self.success?(false)
            }
        }
    }
    func getPlanDetails(id:String){
        ProgressView.startLoader("")
        self.id = id
        NetworkClient.getPlanDetails(id: id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.planDetailsArr = model.details?.first?.heading ?? []
                    self.planDetailSuccess?(true)
                }else{
                    self.planDetailSuccess?(false)
                }
            case .failure(let err):
                print(err)
                self.planDetailSuccess?(false)
            }
        }
    }
    func updatePlanStatus(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.updatePlanStatus(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.getPlanDetails(id: self.id)
                    self.updateSuccess?(model.message ?? "")
                }else{
                    self.updateSuccess?(model.message ?? "")
                    ProgressView.stopLoader()
                }
            case .failure(let err):
                self.updateSuccess?(err.localizedDescription)
                ProgressView.stopLoader()
            }
        }
    }
    func deletePlan(id:String,cat_id:String){
        ProgressView.startLoader("")
        NetworkClient.deletePlan(id: id, cat_id: cat_id){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.getPlanDetails(id: self.id)
                    self.updateSuccess?(model.message ?? "")
                }else{
                    self.updateSuccess?(model.message ?? "")
                    ProgressView.stopLoader()
                }
            case .failure(let err):
                self.updateSuccess?(err.localizedDescription)
                ProgressView.stopLoader()
            }
        }
    }
    func createNewPlans(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.createNewPlans(dataDict: dataDict){ result in
            switch result {
            case .success(let model):
                self.createPlanSuccess?(model.message ?? "")
            case .failure(let err):
                self.createPlanSuccess?(err.localizedDescription)
            }
            
        }
    }
}

