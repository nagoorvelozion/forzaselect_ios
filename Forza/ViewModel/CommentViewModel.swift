//
//  CommentViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 06/04/21.
//

import Foundation

class CommentViewModel:NSObject{
    var commentSuccess : (()->())?
    var addCommentSuccess : ((String)->())?

    var commentArr = [CommentDetailsModel]()
}

extension CommentViewModel{
    
    func displayCommentAPI(planId:String,carrierId:String){
        ProgressView.startLoader("")
        NetworkClient.getDisplayComments(planId: planId, CarrierId: carrierId){ result in
            switch result{
            case .success(let model):
                if model.status == 1{
                    self.commentArr = model.details ?? []
                }
                self.commentSuccess?()
            case .failure(let err):
                print(err)
            }
        }
    }
    func addCommentAPI(dataDict:[String:Any]){
        ProgressView.startLoader("")
        NetworkClient.addComment(dataDict: dataDict){ result in
            switch result{
            case .success(let model):
                self.addCommentSuccess?(model.message ?? "")
            case .failure(let err):
                self.addCommentSuccess?(err.localizedDescription)
            }
        }
    }

}
