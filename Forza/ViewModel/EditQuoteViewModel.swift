//
//  EditQuoteViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 11/04/21.
//

import Foundation

class EditQuoteViewModel:NSObject{
    var success : ((String,Bool)->())?
    var updateSuccess : ((String)->())?
    var editQuoteArr = [EditQuoteDetailsModel]()
}
extension EditQuoteViewModel{
    func getEditQuoteList(id:String,CatId:String){
        ProgressView.startLoader("")
        NetworkClient.getEditQuoteParameters(id: id, CategoryId: CatId){ result in
            switch result{
            case.success(let model):
                if model.status == 1{
                    self.editQuoteArr = model.details ?? []
                    self.success?(model.message ?? "",true)
                }else{
                    self.success?(model.message ?? "",false)
                }
            case .failure(let err):
                self.success?(err.localizedDescription,false)
            }
        }
    }
    func updateProductPrice(id:String,value:String,isFromEditName:Bool){
        ProgressView.startLoader("")
        NetworkClient.updatePriceQuoteParameters(id: id, value: value, isFromEditName: isFromEditName){ result in
            switch result{
            case .success(let model):
                self.updateSuccess?(model.message ?? "")
            case .failure(let err):
                self.updateSuccess?(err.localizedDescription)
            }
        }
    }
}
