//
//  HelpViewModel.swift
//  Forza
//
//  Created by Naveen Kumar K N on 20/05/21.
//

import Foundation

class HelpViewModel{
    var helpArr = [HelpDetailsModel]()
    var success : ((Bool,String)->())?
    var submitSuccess : ((Bool,String)->())?

}

extension HelpViewModel{
    func apiCallForHelpList(){
        ProgressView.startLoader("")
        NetworkClient.getHelpListList(){ response in
            switch response{
            case .success(let model):
                if model.status == 1{
                    self.helpArr = model.details ?? []
                    self.success?(true,model.message ?? "")
                }else{
                    self.success?(false,model.message ?? "")
                }
            case .failure(let err):
                self.success?(false,err.localizedDescription)
            }
        }
    }
    func apiCallForHelpSubmit(information:String){
        ProgressView.startLoader("")
        NetworkClient.submitHelpReasonList(reason: information){ response in
            switch response{
            case .success(let model):
                if model.status == 1{
                    self.submitSuccess?(true,model.message ?? "")
                }else{
                    self.submitSuccess?(false,model.message ?? "")
                }
            case .failure(let err):
                self.submitSuccess?(true,err.localizedDescription)
            }
        }
    }
}
