//
//  Utility.swift
//  Forza
//
//  Created by Naveen Kumar K N on 31/03/21.
//

import Foundation
import UIKit

@objc class Utility:NSObject{
    
    class func showAlertWith(message:String,title:String,forController controller:UIViewController) {
        let alertDisapperTimeInSeconds = 2.0
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .actionSheet)
        alert.view.layer.cornerRadius = 20
        alert.view.backgroundColor = .white
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + alertDisapperTimeInSeconds) {
          alert.dismiss(animated: true)
        }
        controller.present(alert, animated: true)
    }
    
    class   func showAlertWithCompletion(message:String,title:String,okTitle:String,cancelTitle:String?,forController controller:UIViewController,completionBlock:@escaping (_ okPressed:Bool)->()){
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: okTitle, style: .default) { (ok) in
            completionBlock(true)
        }
        okAction.setValue(UIColor(named: "AppColor"), forKey: "titleTextColor")
        
        alertController.addAction(okAction)
        if let cancelTitle = cancelTitle{
            let cancelOption = UIAlertAction(title: cancelTitle, style: .default, handler: { (axn) in
                completionBlock(false)
                
            })
            cancelOption.setValue(UIColor(named: "AppColor"), forKey: "titleTextColor")
            alertController.addAction(cancelOption)
        }
        controller.present(alertController, animated: true, completion: nil)
    }
    
    class func imageRotation()-> CABasicAnimation{
        var rotationAnimation: CABasicAnimation?
        rotationAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotationAnimation?.toValue = Int(.pi * 2.0 * 2 * 0.6)
        rotationAnimation?.duration = 1
        rotationAnimation?.isCumulative = true
        rotationAnimation?.repeatCount = MAXFLOAT
        return rotationAnimation!
    }
}

@IBDesignable
public class Gradient: UIView {
    @IBInspectable var startColor:   UIColor = .black { didSet { updateColors() }}
    @IBInspectable var endColor:     UIColor = .white { didSet { updateColors() }}
    @IBInspectable var startLocation: Double =   0.05 { didSet { updateLocations() }}
    @IBInspectable var endLocation:   Double =   0.95 { didSet { updateLocations() }}
    @IBInspectable var horizontalMode:  Bool =  false { didSet { updatePoints() }}
    @IBInspectable var diagonalMode:    Bool =  false { didSet { updatePoints() }}

    override public class var layerClass: AnyClass { CAGradientLayer.self }

    var gradientLayer: CAGradientLayer { layer as! CAGradientLayer }

    func updatePoints() {
        if horizontalMode {
            gradientLayer.startPoint = diagonalMode ? .init(x: 1, y: 0) : .init(x: 0, y: 0.5)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 0, y: 1) : .init(x: 1, y: 0.5)
        } else {
            gradientLayer.startPoint = diagonalMode ? .init(x: 0, y: 0) : .init(x: 0.5, y: 0)
            gradientLayer.endPoint   = diagonalMode ? .init(x: 1, y: 1) : .init(x: 0.5, y: 1)
        }
    }
    func updateLocations() {
        gradientLayer.locations = [startLocation as NSNumber, endLocation as NSNumber]
    }
    func updateColors() {
        gradientLayer.colors = [startColor.cgColor, endColor.cgColor]
    }
    override public func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        updatePoints()
        updateLocations()
        updateColors()
    }

}

class UserDefaultsManager {
    static var appVersion: String {
        get {
            return UserDefaults.standard.string(forKey: "appVersion") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "appVersion")
        }
    }
    static var Access_Token: String {
        get {
            return UserDefaults.standard.string(forKey: "Access_Token") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "Access_Token")
        }
    }
    static var SessionId: String {
        get {
            return UserDefaults.standard.string(forKey: "SessionId") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "SessionId")
        }
    }
    static var deviceToken: String {
        get {
            return UserDefaults.standard.string(forKey: "deviceToken") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "deviceToken")
        }
    }
    static var userId: String {
        get {
            return UserDefaults.standard.string(forKey: "userId") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "userId")
        }
    }
    static var pwd: String {
        get {
            return UserDefaults.standard.string(forKey: "pwd") ?? ""
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "pwd")
        }
    }

    static var isUserLoggedIn: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isUserLoggedIn")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isUserLoggedIn")
        }
    }
    static var isUserLoggedAsCarrier: Bool {
        get {
            return UserDefaults.standard.bool(forKey: "isUserLoggedAsCarrier")
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "isUserLoggedAsCarrier")
        }
    }
    static var notificationCount: String {
        get {
            return UserDefaults.standard.string(forKey: "notificationCount") ?? "0"
        }
        set {
            UserDefaults.standard.set(newValue, forKey: "notificationCount")
        }
    }
    
}

