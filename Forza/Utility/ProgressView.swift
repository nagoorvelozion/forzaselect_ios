//
//  ProgressView.swift
//  Forza
//
//  Created by Naveenkumar.KN on 01/04/21.
//  Copyright © 2020 Naveen Kumar K N. All rights reserved.
//

import UIKit

@objc class ProgressView: UIView {
    
    @IBOutlet weak var textLable: UILabel!
    @IBOutlet weak var progressImg: UIImageView!
    
    static let ProgressLoaderView : ProgressView = UINib(nibName: "ProgressView", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! ProgressView
    
    class func startLoader(_ text: String) {
        let progressView: ProgressView? = self.ProgressLoaderView
        let mainWindow = UIApplication.shared.windows.first ?? UIWindow()
        progressView?.progressImg.image = progressView?.progressImg.image?.withRenderingMode(.alwaysTemplate)
        progressView?.progressImg.tintColor = .white
        progressView?.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: ((mainWindow.frame.size.width)), height: ((mainWindow.frame.size.height)))
        mainWindow.addSubview(progressView!)
        progressView?.textLable.text = text
        progressView?.progressImg?.layer.add(Utility.imageRotation(), forKey: "rotationAnimation")
    }
    
    class func stopLoader() {
        let progressView: ProgressView? = ProgressLoaderView
        UIView.animate(withDuration: 0.5, animations: {() -> Void in
            progressView?.alpha = 0
        }, completion: {(_ finished: Bool) -> Void in
            progressView?.removeFromSuperview()
            progressView?.alpha = 1
        })
    }
}
