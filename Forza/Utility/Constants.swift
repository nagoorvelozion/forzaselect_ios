//
//  Constants.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import Foundation
import UIKit


let APP_DELEGATE : AppDelegate = UIApplication.shared.delegate as! AppDelegate

let baseURL = "http://www.forzaselect.com/api/"

enum HTTPHeaderField: String {
    case authentication = "Authorization"
    case contentType = "Content-Type"
    case acceptType = "Accept"
    case acceptEncoding = "Accept-Encoding"
}

enum ContentType: String {
    case json = "application/x-www-form-urlencoded"
}
struct K {
    struct ServerURL {
        static let serverbaseURL = baseURL
    }
}

enum CustomFontType: String {
    case RobotoBold = "Roboto-Bold",
         RobotoMedium = "Roboto-Medium",
         RobotoRegular = "Roboto-Regular"
}
