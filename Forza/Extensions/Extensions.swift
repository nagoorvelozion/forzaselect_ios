//
//  Extensions.swift
//  Forza
//
//  Created by Naveen Kumar K N on 01/04/21.
//

import Foundation
import UIKit

extension UITextField{
   @IBInspectable var placeHolderColor: UIColor? {
        get {
            return self.placeHolderColor
        }
        set {
            self.attributedPlaceholder = NSAttributedString(string:self.placeholder != nil ? self.placeholder! : "", attributes:[NSAttributedString.Key.foregroundColor: newValue!])
        }
    }
}

//MARK:- Navigation Related Stufs
enum CustomNavType :String{
    case navPlain
    case navWithBack
    case navWithBellandLogout
    case navWithBackandEdit
}

// Nav Back Button
class NavBackButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setImage(UIImage(named: "BackArrow"), for: .normal)
    }
}
class NavBackButtonWithAppColor: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let image = UIImage(named: "BackArrowAppColor")?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
    }
}
class NavCartButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setImage(UIImage(named: "Plus"), for: .normal)
        self.contentMode = .center
        self.imageView?.contentMode = .scaleAspectFit
    }
}
class NavLogoutButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let image = UIImage(named: "MenuLogout")
        self.setImage(image, for: .normal)
        self.imageView?.contentMode = .scaleAspectFill
        self.contentVerticalAlignment = .center
        self.frame = CGRect(x: 20, y: 10, width: 30, height: 30)
        self.tintColor = .white

    }
}
class NavNotificationButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        let image = UIImage(named: "notification")?.withRenderingMode(.alwaysTemplate)
        self.setImage(image, for: .normal)
    }
}
class NavBellButton: UIButton {
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setImage(UIImage(named: "power"), for: .normal)
        self.contentMode = .center
        self.imageView?.contentMode = .scaleAspectFit
    }
}
// MARK: - Get Required Font
extension UIFont {
    static func getFont(with type: CustomFontType, size: CGFloat) -> UIFont {
        let font = UIFont(name: type.rawValue, size: size) ?? UIFont()
        return font
    }
}
// Nav Menu button
class NavMenuButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
    self.setImage(UIImage(named: "menu"), for: .normal)
        self.contentMode = .center
        self.imageView?.contentMode = .scaleAspectFit

    }
}
class NavDoneButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setTitle("OK", for: .normal)
        self.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        self.contentHorizontalAlignment = .left
    }
}
class NavEditButton: UIButton {
    override func layoutSubviews() {
        super.layoutSubviews()
        self.setTitle("Edit", for: .normal)
        self.frame = CGRect(x: 0, y: 0, width: 100, height: 40)
        self.contentHorizontalAlignment = .right
    }
}

@IBDesignable extension UIView {
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let color = layer.borderColor else {
                return nil
            }
            return UIColor(cgColor: color)
        }
    }
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    @IBInspectable var cornerRadius: CGFloat {
        set {
            layer.cornerRadius = newValue
            clipsToBounds = newValue > 0
        }
        get {
            return layer.cornerRadius
        }
    }
 
}

// MARK: - Custom UITabBarController for Admin Screens TabBarViewController

@IBDesignable class DashboardTabBarViewController: UITabBarController, UITabBarControllerDelegate{
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0, green: 0.1582030654, blue: 0.2257892191, alpha: 1)
//        let selectedColor   = UIColor(red: 246.0/255.0, green: 155.0/255.0, blue: 13.0/255.0, alpha: 1.0)
//           let unselectedColor = UIColor(red: 16.0/255.0, green: 224.0/255.0, blue: 223.0/255.0, alpha: 1.0)
//
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: unselectedColor], for: .normal)
//        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedString.Key.foregroundColor: selectedColor], for: .selected)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}
// MARK: - Custom UITabBarController for Carrier Screens TabBarViewController

@IBDesignable class CarrierTabBarViewController: UITabBarController, UITabBarControllerDelegate{
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        self.delegate = self
        tabBar.unselectedItemTintColor = #colorLiteral(red: 0, green: 0.1582030654, blue: 0.2257892191, alpha: 1)
        
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
    }
}

class SSBadgeButton: UIButton {
    
    var badgeLabel = UILabel()
    
    var badge: String? {
        didSet {
            addBadgeToButon(badge: badge)
        }
    }

    public var badgeBackgroundColor = UIColor.red {
        didSet {
            badgeLabel.backgroundColor = badgeBackgroundColor
        }
    }
    
    public var badgeTextColor = UIColor.white {
        didSet {
            badgeLabel.textColor = badgeTextColor
        }
    }
    
    public var badgeFont = UIFont.systemFont(ofSize: 12.0) {
        didSet {
            badgeLabel.font = badgeFont
        }
    }
    
    public var badgeEdgeInsets: UIEdgeInsets? {
        didSet {
            addBadgeToButon(badge: badge)
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        addBadgeToButon(badge: nil)
    }
    
    func addBadgeToButon(badge: String?) {
        badgeLabel.text = badge
        badgeLabel.textColor = badgeTextColor
        badgeLabel.backgroundColor = badgeBackgroundColor
        badgeLabel.font = badgeFont
        badgeLabel.sizeToFit()
        badgeLabel.textAlignment = .center
        let badgeSize = badgeLabel.frame.size
        
        let height = max(18, Double(badgeSize.height) + 5.0)
        let width = max(height, Double(badgeSize.width) + 10.0)
        
        var vertical: Double?, horizontal: Double?
        if let badgeInset = self.badgeEdgeInsets {
            vertical = Double(badgeInset.top) - Double(badgeInset.bottom)
            horizontal = Double(badgeInset.left) - Double(badgeInset.right)
            
            let x = (Double(bounds.size.width) - 10 + horizontal!)
            let y = -(Double(badgeSize.height) / 2) - 10 + vertical!
            badgeLabel.frame = CGRect(x: x, y: y, width: width, height: height)
        } else {
            let x = self.frame.width - CGFloat((width / 2.0))
            let y = CGFloat(-(height / 2.0))
            badgeLabel.frame = CGRect(x: x, y: y, width: CGFloat(width), height: CGFloat(height))
        }
        
        badgeLabel.layer.cornerRadius = badgeLabel.frame.height/2
        badgeLabel.layer.masksToBounds = true
        addSubview(badgeLabel)
        badgeLabel.isHidden = badge != nil ? false : true
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.addBadgeToButon(badge: nil)
      //  fatalError("init(coder:) has not been implemented")
    }
}

extension Encodable {

    /// Encode into JSON and return `Data`
    func jsonData() throws -> Data {
        let encoder = JSONEncoder()
        encoder.outputFormatting = .prettyPrinted
        encoder.dateEncodingStrategy = .iso8601
        return try encoder.encode(self)
    }
}
